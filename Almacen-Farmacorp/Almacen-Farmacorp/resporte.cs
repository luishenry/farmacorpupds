﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Capanegocio;
namespace Almacen_Farmacorp
{
    public partial class resporte : Form
    {
        public List<Cfactura> datos = new List<Cfactura>();
        public List<Cfactura> datosl = new List<Cfactura>();
        public resporte()
        {
            InitializeComponent();
        }

        private void resporte_Load(object sender, EventArgs e)
        {
            reportViewer1.LocalReport.DataSources.Clear();
            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet1",datos));

            reportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WinForms.ReportDataSource("DataSet2", datosl));
            this.reportViewer1.RefreshReport();
        }
    }
}
