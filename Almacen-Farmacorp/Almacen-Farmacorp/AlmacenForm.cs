﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class AlmacenForm : Form
    {
        Capanegocio.CAlmacen Calmacen = new Capanegocio.CAlmacen();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        string operacion = "Insertar";
        string idalmaupdate;
        public AlmacenForm()
        {
            InitializeComponent();
        }

        private void AlmacenForm_Load(object sender, EventArgs e)
        {
            actualizarForm();
        }
        public void actualizarForm()
        {
            TraerAlmacen();
        }
        public void TraerAlmacen()
        {
            DataTable alma = new DataTable();
            alma = Calmacen.traerAlmacen();
            dataGridView1.DataSource = alma;

        }
        string idalmacen;
        public void generarCodigoAlmacen()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = Calmacen.generaridAlmacen();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idalmacen = l[0][0].ToString();
                }

                else
                {

                    idalmacen = "1";
                }
            }
            catch
            {
                idalmacen = "1";
            }

        }
        public bool insertarAlmacen(object tr)
        {
            generarCodigoAlmacen();
            Calmacen.v[0] = idalmacen;
            Calmacen.v[1] = txtname.Text;
            Calmacen.v[2] = txtdireccion.Text;

            if (Calmacen.InsertAlmacen(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Proveedor");
                return false;
            }
            return true;
        }
        public bool actualizarAlmacen(object tr, string id)
        {
            Calmacen.v2[0] = id;
            Calmacen.v2[1] = txtname.Text;
            Calmacen.v2[2] = txtdireccion.Text;
            if (Calmacen.ActualizarAlmacen(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Proveedor");
                return false;
            }
            return true;
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (operacion == "Insertar")
            {
                var tr = CC.iniTR();
                if (insertarAlmacen(tr))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                }
            }
            else if (operacion == "editar")
            {
                var tr = CC.iniTR();
                if (actualizarAlmacen(tr, idalmaupdate))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                    operacion = "Insertar";
                }
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                operacion = "editar";
                txtname.Text = dataGridView1.CurrentRow.Cells["nombre_almacen"].Value.ToString();
                txtdireccion.Text = dataGridView1.CurrentRow.Cells["direccion_almacen"].Value.ToString();
                idalmaupdate = dataGridView1.CurrentRow.Cells["idalmacen"].Value.ToString();
            }
            else
            {
                MessageBox.Show("debe seleccionar una fila");
            }
        }
    }
}
