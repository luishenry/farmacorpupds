﻿namespace Almacen_Farmacorp
{
    partial class AgregarCategoriaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbcategoria = new System.Windows.Forms.ComboBox();
            this.cbsubcategoria = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcategoria = new System.Windows.Forms.TextBox();
            this.txtsubcategoria = new System.Windows.Forms.TextBox();
            this.btncategoria = new System.Windows.Forms.Button();
            this.btnsubcategoria = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbcategoria
            // 
            this.cbcategoria.FormattingEnabled = true;
            this.cbcategoria.Location = new System.Drawing.Point(24, 63);
            this.cbcategoria.Name = "cbcategoria";
            this.cbcategoria.Size = new System.Drawing.Size(183, 24);
            this.cbcategoria.TabIndex = 0;
            this.cbcategoria.SelectedIndexChanged += new System.EventHandler(this.cbcategoria_SelectedIndexChanged_1);
            // 
            // cbsubcategoria
            // 
            this.cbsubcategoria.FormattingEnabled = true;
            this.cbsubcategoria.Location = new System.Drawing.Point(24, 165);
            this.cbsubcategoria.Name = "cbsubcategoria";
            this.cbsubcategoria.Size = new System.Drawing.Size(183, 24);
            this.cbsubcategoria.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "Categorias";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Subcategorias";
            // 
            // txtcategoria
            // 
            this.txtcategoria.Location = new System.Drawing.Point(292, 64);
            this.txtcategoria.Name = "txtcategoria";
            this.txtcategoria.Size = new System.Drawing.Size(176, 22);
            this.txtcategoria.TabIndex = 4;
            // 
            // txtsubcategoria
            // 
            this.txtsubcategoria.Location = new System.Drawing.Point(292, 165);
            this.txtsubcategoria.Name = "txtsubcategoria";
            this.txtsubcategoria.Size = new System.Drawing.Size(176, 22);
            this.txtsubcategoria.TabIndex = 5;
            // 
            // btncategoria
            // 
            this.btncategoria.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btncategoria.Location = new System.Drawing.Point(311, 93);
            this.btncategoria.Name = "btncategoria";
            this.btncategoria.Size = new System.Drawing.Size(157, 33);
            this.btncategoria.TabIndex = 6;
            this.btncategoria.Text = "Agregar Categoria";
            this.btncategoria.UseVisualStyleBackColor = false;
            this.btncategoria.Click += new System.EventHandler(this.btncategoria_Click);
            // 
            // btnsubcategoria
            // 
            this.btnsubcategoria.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnsubcategoria.Location = new System.Drawing.Point(311, 194);
            this.btnsubcategoria.Name = "btnsubcategoria";
            this.btnsubcategoria.Size = new System.Drawing.Size(157, 35);
            this.btnsubcategoria.TabIndex = 7;
            this.btnsubcategoria.Text = "Agregar SubCategoria";
            this.btnsubcategoria.UseVisualStyleBackColor = false;
            this.btnsubcategoria.Click += new System.EventHandler(this.btnsubcategoria_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(211, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 62);
            this.button1.TabIndex = 8;
            this.button1.Text = "Agregar Nueva";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(213, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 62);
            this.button2.TabIndex = 9;
            this.button2.Text = "Agregar Nueva";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AgregarCategoriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 241);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnsubcategoria);
            this.Controls.Add(this.btncategoria);
            this.Controls.Add(this.txtsubcategoria);
            this.Controls.Add(this.txtcategoria);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbsubcategoria);
            this.Controls.Add(this.cbcategoria);
            this.Name = "AgregarCategoriaForm";
            this.Text = "AgregarCategoriaForm";
            this.Load += new System.EventHandler(this.AgregarCategoriaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbcategoria;
        private System.Windows.Forms.ComboBox cbsubcategoria;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcategoria;
        private System.Windows.Forms.TextBox txtsubcategoria;
        private System.Windows.Forms.Button btncategoria;
        private System.Windows.Forms.Button btnsubcategoria;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}