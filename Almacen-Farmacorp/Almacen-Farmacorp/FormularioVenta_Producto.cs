﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Capanegocio;
using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System.IO;
using System.Drawing.Imaging;
using Microsoft.CSharp;
namespace Almacen_Farmacorp
{
    public partial class FormularioVenta_Producto : Form
    {
        public FormularioVenta_Producto()
        {
            InitializeComponent();
        }

        Capacontrol.CControl cc = new Capacontrol.CControl();
        Capanegocio.CCliente Ccl = new Capanegocio.CCliente();
        Capanegocio.CVentas CV = new Capanegocio.CVentas();

        public string traerFarmacorp() {

            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = CV.traersucursalid(txtidprincipal.Text);

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
         
              
                    return l[0][0].ToString();
            
       

 


        }
        private void FormularioVenta_Producto_Load(object sender, EventArgs e)
        {
         
            generarpedido();
            txtfecha.Text = DateTime.Today.ToString("dd/MM/yy, hh:mm:ss");
            txtidpedido.Text = idpedido;
        }

        private void BtnAgregarCliente_Click(object sender, EventArgs e)
        {
            ClienteForm f = new ClienteForm();
            f.Show();
        }
        public void Cliente_nit_buscar() {

            DataTable d = new DataTable();
            d = Ccl.TraerClienteNit(txtbuscarnit.Text,txtbuscarnit.Text);
            dtGridCliente.DataSource = d;

        }
        private void txtbuscarnit_KeyUp(object sender, KeyEventArgs e)
        {
            Cliente_nit_buscar();
        }
        public void buscar_producto() {

            DataTable d = new DataTable();
            d = CV.TraerProducto_id_nombre(txtbuscarproducto.Text, txtbuscarproducto.Text);
            dtgridProductos.DataSource = d;

        }
        private void txtbuscarproducto_KeyUp(object sender, KeyEventArgs e)
        {
            buscar_producto();
        }
        string idpedido;
        public void generarpedido()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = CV.GenerarIdPedido();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idpedido = l[0][0].ToString();
                }

                else
                {

                    idpedido = "1";
                }
            }
            catch
            {
                idpedido = "1";
            }

        }
        string idCliente;
        string nitCliente;
        private void dtGridCliente_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex==0) {
                try
                {
                    idCliente = dtGridCliente.CurrentRow.Cells["IdCliente"].Value.ToString();
                    txtcliente.Text = dtGridCliente.CurrentRow.Cells["Nombre"].Value.ToString();
                    txt_nit_cliente.Text= dtGridCliente.CurrentRow.Cells["nit"].Value.ToString();
                }
                catch { MessageBox.Show("seleccione un cliente"); }
            }
        }
        int r = -1;
        private void dtgridProductos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) {

                try {
                    bool exist = dtGridCarrito.Rows.Cast<DataGridViewRow>().Any(row => Convert.ToString(row.Cells["idProducto_"].Value) == dtgridProductos.CurrentRow.Cells["idProducto"].Value.ToString());
                    int SumaCantidades = 0;
                    if (!exist)
                    {
                        if (int.Parse(dtgridProductos.CurrentRow.Cells["cantidad"].Value.ToString()) > 0)
                        {
                            if (int.Parse(dtgridProductos.CurrentRow.Cells["cantidad"].Value.ToString()) <= int.Parse(dtgridProductos.CurrentRow.Cells["stock"].Value.ToString()))
                            {
                                dtGridCarrito.Rows.Add();
                                r++;
                                dtGridCarrito.Rows[r].Cells[1].Value = dtgridProductos.CurrentRow.Cells["producto"].Value;
                                dtGridCarrito.Rows[r].Cells[2].Value = dtgridProductos.CurrentRow.Cells["cantidad"].Value;
                                dtGridCarrito.Rows[r].Cells[3].Value = dtgridProductos.CurrentRow.Cells["precio_venta"].Value;
                                dtGridCarrito.Rows[r].Cells[4].Value = (double.Parse(dtGridCarrito.Rows[r].Cells[2].Value.ToString()) * double.Parse(dtGridCarrito.Rows[r].Cells[3].Value.ToString())).ToString();
                                dtGridCarrito.Rows[r].Cells[5].Value = txtidpedido.Text;
                                dtGridCarrito.Rows[r].Cells[6].Value = dtgridProductos.CurrentRow.Cells["idAlmacen"].Value;
                                dtGridCarrito.Rows[r].Cells[7].Value = dtgridProductos.CurrentRow.Cells["idProducto"].Value;
                            
                                for (int i = 0; i<dtGridCarrito.RowCount - 1; i++)
                                {
                                    SumaCantidades = SumaCantidades +int.Parse(dtGridCarrito.Rows[i].Cells[2].Value.ToString());
                                }


                                txtmonto.Text =SumaCantidades.ToString();
                                dtgridProductos.CurrentRow.Cells["cantidad"].Value = 0;
                            }
                            else { MessageBox.Show("no puede sobrepasar el limite del stock"); }
                        }
                        else { MessageBox.Show("no puede ingresar cantidades nulas o menores a 1"); }
                    }
                    else if(exist){

                        for(int i =0; i<dtGridCarrito.RowCount -1; i++)
                        {

                            if (dtGridCarrito.Rows[i].Cells[7].Value.ToString() == dtgridProductos.CurrentRow.Cells["idProducto"].Value.ToString())
                            {
                                int comparar= int.Parse(dtGridCarrito.Rows[i].Cells[2].Value.ToString()) + int.Parse(dtgridProductos.CurrentRow.Cells["cantidad"].Value.ToString());
                                int compararstock = 0;

                                for (int c = 0; c < dtgridProductos.Rows.Count - 1; c++) {

                                    if(dtgridProductos.CurrentRow.Cells["idProducto"].Value.ToString()== dtgridProductos.Rows[c].Cells[5].Value.ToString())
                                    compararstock = compararstock+ int.Parse(dtgridProductos.Rows[c].Cells[2].Value.ToString());


                                }

                                if (compararstock >= comparar)
                                {
                                    dtGridCarrito.Rows[i].Cells[2].Value = (int.Parse(dtGridCarrito.Rows[i].Cells[2].Value.ToString()) + int.Parse(dtgridProductos.CurrentRow.Cells["cantidad"].Value.ToString())).ToString();
                                    dtGridCarrito.Rows[i].Cells[4].Value = (double.Parse(dtGridCarrito.Rows[i].Cells[2].Value.ToString()) * double.Parse(dtGridCarrito.Rows[i].Cells[3].Value.ToString())).ToString();
                                    dtgridProductos.CurrentRow.Cells["cantidad"].Value = 0;

                                    for (int j = 0; j < dtGridCarrito.RowCount - 1; j++)
                                    {
                                        SumaCantidades = SumaCantidades + int.Parse(dtGridCarrito.Rows[j].Cells[2].Value.ToString());
                                    }
                                }
                                else { MessageBox.Show("no puede sobrepasar el limite del stock"); }

                            }
                            else { }


                        }

                    }

                }
                catch { MessageBox.Show("Valores invalidos"); }



            }
        }

        public bool DatoPedidoEntrega() {
            var tr = cc.iniTR();
            if (Insertarpedido(tr)) {

                cc.finTR(tr);
                return true;
            }


            return false;
        }
        Capanegocio.CUsuario us = new Capanegocio.CUsuario();
        public int idempleadogenerado()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = us.traerid(txtidprincipal.Text);

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    return int.Parse(l[0][0].ToString());
                }

                else
                {

                    return 0;
                }
            }
            catch
            {
                return 0;
            }

        }

        string idfactura;

        bool Insertarpedido(object tr) {



            idfactura= CV.v1[0] = idpedido;
            CV.v1[1] = DateTime.Today.ToString("MM/dd/yy");
            CV.v1[2] = txtmonto.Text;
            CV.v1[3] = idCliente;
            CV.v1[4] = idempleadogenerado().ToString();
            if (CV.InsertarPedido(tr) == 0) {

                cc.deshacerTR(tr);
                MessageBox.Show("error en el pedido");
            }

            return true;
        }

        bool DatoPedidoDetalleEntrega() {
            var tr = cc.iniTR();

            if (PedidoDetalleEntrega(tr)) {

                cc.finTR(tr);
                return true;
            }

            return false;
        }
        bool PedidoDetalleEntrega(object tr) {

            for (int i = 0; i < dtGridCarrito.RowCount - 1; i++)
            {
                CV.v2[0] = dtGridCarrito.Rows[i].Cells[2].Value.ToString();
                CV.v2[1]= dtGridCarrito.Rows[i].Cells[3].Value.ToString().Replace(",", ".");
                CV.v2[2] = idpedido;
                CV.v2[3] = dtGridCarrito.Rows[i].Cells[6].Value.ToString()  ;
                CV.v2[4] = dtGridCarrito.Rows[i].Cells[7].Value.ToString();
                if (CV.InsertarDetallePedido(tr) == 0)
                {
                    cc.deshacerTR(tr);
                    MessageBox.Show("error en el detalle de producto");
                    return false;
                }
            }
            return true;
        }

        //este Algoritmo genera factura, extrayendo datos desde el formulario cuando el cliente sea registrado con el dataset 1
        //inicio

        string TextoQr;
        public void GenerarFactura() {
            TextoQr = "";

            Cfactura f = new Cfactura();
            string cerosreceptor = "";
            for (int i = 0;i<= 13 - txtNit_empresa.Text.Length; i++) {
                cerosreceptor = cerosreceptor + "0";

            }

            string ceros_nrofactura = "";
            for (int i = 0; i <= 13 - txtnro_factura.Text.Length; i++)
            {
                ceros_nrofactura = ceros_nrofactura + "0";

            }
            f.facturaNro = ceros_nrofactura+txtnro_factura.Text;



            f.nitEmpresa = cerosreceptor+txtNit_empresa.Text;
            f.autorizacion = txtnro_autorizacion.Text;

            f.nitCliente = txt_nit_cliente.Text;
            f.fecha = DateTime.Today.ToString("dd/MM/yy, hh-mm-ss");
            f.NombreCliente = txtcliente.Text;
            f.Codigo_control = txt_cod_autorizacion.Text;
            TextoQr =f.nitEmpresa+DateTime.Now.ToString("yyyyMMddHHmmssfff") +"0000"+"2"+"2"+"1"+"01"+f.facturaNro+"0000";
            f.CodigoQr = GenerarQR();
            double total_pagar=0;
            for (int i = 0; i < dtGridCarrito.Rows.Count - 1; i++) {


                total_pagar= total_pagar + double.Parse(dtGridCarrito.Rows[i].Cells[3].Value.ToString());

            }
            f.monto_total = total_pagar.ToString();
         
            resporte reporte = new resporte();
//esta parte del try catch esta traendome toda la lista da productos dentro del reporte, con el dataset2
            try
            {
                for (int i = 0; i <dtGridCarrito.Rows.Count - 1; i++)
                {
                    //esto es datosL ingresa datos del datagrid haciendo un recorido de todos los datos dentro del datagridview
                    reporte.datosl.Add(new Cfactura
                    {

                        Productos = dtGridCarrito.Rows[i].Cells[1].Value.ToString(),
                        ventas=dtGridCarrito.Rows[i].Cells[3].Value.ToString()


                    });
             
                }

                
            }
            catch { MessageBox.Show("Rellene el campo Descripcion"); }
   
            //esto es datos 1 agrega datos de textbox,variables y fecha, no inclute el datagrid
            reporte.datos.Add(f);

        

            reporte.Show();

        }

        //Fin
        bool DatosActualizarDetallePedido_proveedor() {
            var tr = cc.iniTR();

            if (ActualizarDetallePedido_proveedor(tr)) {
                cc.finTR(tr);
                return true;
            }

            return false;
        }




        public bool ActualizarDetallePedido_proveedor(object tr) {


            for (int i = 0; i < dtGridCarrito.Rows.Count - 1; i++) {
                CV.VPed[0] = dtGridCarrito.Rows[i].Cells[2].Value.ToString();
                CV.VPed[1] = dtGridCarrito.Rows[i].Cells[6].Value.ToString();
                CV.VPed[2] = dtGridCarrito.Rows[i].Cells[7].Value.ToString();
               
                if (CV.ActualizarCantidadPedido(tr) == 0)
                {
                    cc.deshacerTR(tr);
                    MessageBox.Show("Error en actualizar pedidos");
                    return false;
                }
            }

            return true;
        }

        public void limpiardatos() {
            dtGridCarrito.DataSource = null;
            dtgridProductos.DataSource = null;
            dtGridCarrito.ClearSelection();
            dtGridCarrito.Rows.Clear();
            dtgridProductos.ClearSelection();
            txtmonto.Text = "0";

            r = -1;
        }
        private void Btn_registrar_Click(object sender, EventArgs e)
        {

   if (DatoPedidoEntrega()) {
                if (DatoPedidoDetalleEntrega()) {

                    if (DatosActualizarDetallePedido_proveedor())
                    {
                        if(datos_factura()){
                     
                            GenerarFactura();
              
                        }
                    }
                }
              
                
 }
        }


        string DatosParaGenerarQR;

        public byte[] GenerarQR() {
            DatosParaGenerarQR = obtenerModulo11(TextoQr);
;

            QrEncoder qrEncoder = new QrEncoder(ErrorCorrectionLevel.H);
            QrCode qrCode = new QrCode();

            qrEncoder.TryEncode(DatosParaGenerarQR, out qrCode);

            GraphicsRenderer renderer = new GraphicsRenderer(new FixedCodeSize(400, QuietZoneModules.Zero), Brushes.Black, Brushes.White);
            MemoryStream ms = new MemoryStream();

            renderer.WriteToStream(qrCode.Matrix, ImageFormat.Png, ms);
            var imageTemporal = new Bitmap(ms);
            var imagen = new Bitmap(imageTemporal, new Size(new Point(200, 200)));


          Image imagein = (Image)imagen.Clone();
            using (var t = new MemoryStream())
            {
                imagein.Save(t, ImageFormat.Png);
                return t.ToArray();
            }
          
            //Guardar en el disco duro la imagen (Carpeta del proyecto)
           
            

        }
        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            Boolean nonNumberEntered;


            nonNumberEntered = true;


            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == 8)
            {
                nonNumberEntered = false;
            }


            if (nonNumberEntered == true)
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }


        }

        private void dtgridProductos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 1)
            {
                e.Control.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxNumeric_KeyPress);

            }
        }

        private void btn_nuevo_Click(object sender, EventArgs e)
        {
            limpiardatos();
        }

        private void dtGridCarrito_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                dtGridCarrito.Rows.RemoveAt(dtGridCarrito.CurrentRow.Index);
                r--;

            }
        }
        Capanegocio.Cfactura_crud fac = new Capanegocio.Cfactura_crud();
        public string generar_nro_factura()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = fac.traernro_factura();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    return l[0][0].ToString();
                }

                else
                {

                    return "1";
                }
            }
            catch
            {
                return "1";
            }

        }

        double montototal() {
            double total = 0;
            for (int i = 0; i < dtGridCarrito.Rows.Count - 1; i++)
            {
                total= total +double.Parse(dtGridCarrito.Rows[i].Cells[3].Value.ToString());


            }
            return total;


        }
        bool datos_factura() {
            var tr = cc.iniTR();

            if (insertar_factura(tr)) {
                cc.finTR(tr);
                return true;

            }
            return false;

        }
        public bool insertar_factura(object tr) {

            fac.v[0] = idfactura;
            fac.v[1] = txtNit_empresa.Text;
            fac.v[2] = generar_nro_factura();
            fac.v[3] = txtnro_autorizacion.Text;
            fac.v[4] = DateTime.Today.ToString("MM/dd/yy");
            fac.v[5] = montototal().ToString().Replace(",",".");
            fac.v[6] = txt_cod_autorizacion.Text;
            if (fac.insertar_factura()==0) {

                cc.deshacerTR(tr);
                MessageBox.Show("error en insersion de factura");

                return false;
            }
            return true;

        }

        public string GetCheckDigit(string number)
        {
            int sum = 0;
            for (int i = number.Length - 1, multiplier = 2; i >= 0; i--)
            {
                sum += (int)char.GetNumericValue(number[i]) * multiplier;
                if (++multiplier > 9) multiplier = 2;
            }
            int mod = (sum % 11);
            if (mod == 0 || mod == 1) return "0";
            return (11 - mod).ToString();
        }
        public static string CalculaDigitoModulo11(string pDato, int pNumDigito, int pLimMultiplo, bool pX10 = false)

        {

            int vMultiplo, vSuma, vDigito = 0;



            if (!pX10)

                pNumDigito = 1;



            for (int i = 1; i <= pNumDigito; i++)

            {

                vSuma = 0;

                vMultiplo = 2;



                for (int n = pDato.Length - 1; n >= 0; n--)

                {

                    vSuma += (vMultiplo * Int32.Parse(pDato.Substring(n, 1)));



                    if (++vMultiplo > pLimMultiplo)

                        vMultiplo = 2;

                }



                if (pX10)

                    vDigito = ((vSuma * 10) % 11) % 10;

                else

                    vDigito = vSuma % 11;



                if (vDigito == 10)

                    pDato += "1";



                if (vDigito == 11)

                    pDato += "0";



                if (vDigito < 10)

                    pDato += vDigito;

            }



            return pDato.Substring(pDato.Length - pNumDigito, 1);

        }


        public String obtenerModulo11(String pCadena)
        {

            String vDigito = CalculaDigitoModulo11(pCadena, 1, 9, false);

            return vDigito;

        }


    }
}
