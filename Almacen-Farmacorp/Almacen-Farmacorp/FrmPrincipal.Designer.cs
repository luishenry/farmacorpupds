﻿namespace Almacen_Farmacorp
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrincipal));
            this.LblUserid = new System.Windows.Forms.Label();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.btnventa = new System.Windows.Forms.Button();
            this.btnCliente = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btn_gestionar = new System.Windows.Forms.Button();
            this.btn_producto = new System.Windows.Forms.Button();
            this.btn_almacen_producto = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_gestionar_proveedor = new System.Windows.Forms.Button();
            this.btn_pedido_proveedor = new System.Windows.Forms.Button();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblcodigo = new System.Windows.Forms.Label();
            this.Btn_session = new System.Windows.Forms.Button();
            this.btn_historial_ventas = new System.Windows.Forms.Button();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // LblUserid
            // 
            this.LblUserid.AutoSize = true;
            this.LblUserid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblUserid.Location = new System.Drawing.Point(435, 82);
            this.LblUserid.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblUserid.Name = "LblUserid";
            this.LblUserid.Size = new System.Drawing.Size(79, 29);
            this.LblUserid.TabIndex = 0;
            this.LblUserid.Text = "label1";
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.btn_historial_ventas);
            this.metroPanel1.Controls.Add(this.btnventa);
            this.metroPanel1.Controls.Add(this.btnCliente);
            this.metroPanel1.Controls.Add(this.button5);
            this.metroPanel1.Controls.Add(this.button4);
            this.metroPanel1.Controls.Add(this.button3);
            this.metroPanel1.Controls.Add(this.btn_gestionar);
            this.metroPanel1.Controls.Add(this.btn_producto);
            this.metroPanel1.Controls.Add(this.btn_almacen_producto);
            this.metroPanel1.Controls.Add(this.button2);
            this.metroPanel1.Controls.Add(this.button1);
            this.metroPanel1.Controls.Add(this.btn_gestionar_proveedor);
            this.metroPanel1.Controls.Add(this.btn_pedido_proveedor);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 15;
            this.metroPanel1.Location = new System.Drawing.Point(6, 123);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(252, 972);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 15;
            // 
            // btnventa
            // 
            this.btnventa.Location = new System.Drawing.Point(0, 625);
            this.btnventa.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnventa.Name = "btnventa";
            this.btnventa.Size = new System.Drawing.Size(248, 46);
            this.btnventa.TabIndex = 10;
            this.btnventa.Text = "Venta_Cliente";
            this.btnventa.UseVisualStyleBackColor = true;
            this.btnventa.Click += new System.EventHandler(this.btnventa_Click);
            // 
            // btnCliente
            // 
            this.btnCliente.Location = new System.Drawing.Point(4, 568);
            this.btnCliente.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(248, 46);
            this.btnCliente.TabIndex = 9;
            this.btnCliente.Text = "gestionar Cliente";
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(4, 512);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(248, 46);
            this.button5.TabIndex = 8;
            this.button5.Text = "Historial de entregas";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(4, 455);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(248, 46);
            this.button4.TabIndex = 4;
            this.button4.Text = "Gestionar sucursal";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(4, 400);
            this.button3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(248, 46);
            this.button3.TabIndex = 4;
            this.button3.Text = "Gestionar Categorias";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // btn_gestionar
            // 
            this.btn_gestionar.Location = new System.Drawing.Point(4, 345);
            this.btn_gestionar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_gestionar.Name = "btn_gestionar";
            this.btn_gestionar.Size = new System.Drawing.Size(248, 46);
            this.btn_gestionar.TabIndex = 3;
            this.btn_gestionar.Text = "Gestionar ciudad";
            this.btn_gestionar.UseVisualStyleBackColor = true;
            this.btn_gestionar.Click += new System.EventHandler(this.btn_gestionar_Click);
            // 
            // btn_producto
            // 
            this.btn_producto.Location = new System.Drawing.Point(4, 109);
            this.btn_producto.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_producto.Name = "btn_producto";
            this.btn_producto.Size = new System.Drawing.Size(248, 48);
            this.btn_producto.TabIndex = 7;
            this.btn_producto.Text = "Gestionar Producto";
            this.btn_producto.UseVisualStyleBackColor = true;
            this.btn_producto.Click += new System.EventHandler(this.btn_producto_Click);
            // 
            // btn_almacen_producto
            // 
            this.btn_almacen_producto.Location = new System.Drawing.Point(4, 168);
            this.btn_almacen_producto.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_almacen_producto.Name = "btn_almacen_producto";
            this.btn_almacen_producto.Size = new System.Drawing.Size(248, 48);
            this.btn_almacen_producto.TabIndex = 6;
            this.btn_almacen_producto.Text = "Almacen_producto";
            this.btn_almacen_producto.UseVisualStyleBackColor = true;
            this.btn_almacen_producto.Click += new System.EventHandler(this.btn_almacen_producto_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(4, 282);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(248, 52);
            this.button2.TabIndex = 4;
            this.button2.Text = "Gestionar Almacen";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(4, 226);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(248, 46);
            this.button1.TabIndex = 2;
            this.button1.Text = "Gestionar Empleado";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_gestionar_proveedor
            // 
            this.btn_gestionar_proveedor.Location = new System.Drawing.Point(4, 5);
            this.btn_gestionar_proveedor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_gestionar_proveedor.Name = "btn_gestionar_proveedor";
            this.btn_gestionar_proveedor.Size = new System.Drawing.Size(248, 42);
            this.btn_gestionar_proveedor.TabIndex = 5;
            this.btn_gestionar_proveedor.Text = "Gestionar Proveedor";
            this.btn_gestionar_proveedor.UseVisualStyleBackColor = true;
            this.btn_gestionar_proveedor.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_pedido_proveedor
            // 
            this.btn_pedido_proveedor.Location = new System.Drawing.Point(4, 52);
            this.btn_pedido_proveedor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_pedido_proveedor.Name = "btn_pedido_proveedor";
            this.btn_pedido_proveedor.Size = new System.Drawing.Size(248, 48);
            this.btn_pedido_proveedor.TabIndex = 3;
            this.btn_pedido_proveedor.Text = "Pedido a proveedor";
            this.btn_pedido_proveedor.UseVisualStyleBackColor = true;
            this.btn_pedido_proveedor.Click += new System.EventHandler(this.btn_pedido_proveedor_Click);
            // 
            // metroPanel2
            // 
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 15;
            this.metroPanel2.Location = new System.Drawing.Point(278, 128);
            this.metroPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(1556, 968);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(332, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Usuario: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(284, 77);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lblcodigo
            // 
            this.lblcodigo.AllowDrop = true;
            this.lblcodigo.AutoSize = true;
            this.lblcodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcodigo.Location = new System.Drawing.Point(910, 82);
            this.lblcodigo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblcodigo.Name = "lblcodigo";
            this.lblcodigo.Size = new System.Drawing.Size(31, 29);
            this.lblcodigo.TabIndex = 4;
            this.lblcodigo.Text = "...";
            this.lblcodigo.Visible = false;
            // 
            // Btn_session
            // 
            this.Btn_session.Location = new System.Drawing.Point(1586, 46);
            this.Btn_session.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Btn_session.Name = "Btn_session";
            this.Btn_session.Size = new System.Drawing.Size(248, 42);
            this.Btn_session.TabIndex = 11;
            this.Btn_session.Text = "Cerrar Sesion";
            this.Btn_session.UseVisualStyleBackColor = true;
            this.Btn_session.Click += new System.EventHandler(this.Btn_session_Click);
            // 
            // btn_historial_ventas
            // 
            this.btn_historial_ventas.Location = new System.Drawing.Point(0, 681);
            this.btn_historial_ventas.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_historial_ventas.Name = "btn_historial_ventas";
            this.btn_historial_ventas.Size = new System.Drawing.Size(248, 46);
            this.btn_historial_ventas.TabIndex = 11;
            this.btn_historial_ventas.Text = "Historial_Ventas_Cliente";
            this.btn_historial_ventas.UseVisualStyleBackColor = true;
            this.btn_historial_ventas.Click += new System.EventHandler(this.btn_historial_ventas_Click);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1884, 1106);
            this.ControlBox = false;
            this.Controls.Add(this.Btn_session);
            this.Controls.Add(this.lblcodigo);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.metroPanel2);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.LblUserid);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmPrincipal";
            this.Padding = new System.Windows.Forms.Padding(30, 92, 30, 31);
            this.Text = " ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPrincipal_Load);
            this.metroPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label LblUserid;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private System.Windows.Forms.Button btn_pedido_proveedor;
        private System.Windows.Forms.Button button1;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private System.Windows.Forms.Button btn_gestionar_proveedor;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_gestionar;
        private System.Windows.Forms.Button btn_producto;
        private System.Windows.Forms.Button btn_almacen_producto;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.Label lblcodigo;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnventa;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Button Btn_session;
        private System.Windows.Forms.Button btn_historial_ventas;
    }
}