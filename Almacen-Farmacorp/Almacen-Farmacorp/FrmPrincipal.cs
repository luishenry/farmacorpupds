﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class FrmPrincipal : MetroFramework.Forms.MetroForm
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }
        Capanegocio.CUsuario us = new Capanegocio.CUsuario();
       public string ValidarRango;
        private void FrmPrincipal_Load(object sender, EventArgs e)
        {
            if (ValidarRango == "1") {

            }
            else if (ValidarRango=="2") {
                button1.Enabled = false;
                button2.Enabled = false;
                btn_gestionar.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                btnCliente.Enabled = false;
                btnventa.Enabled = false;
                button1.Visible = false;
                button2.Visible = false;
                btn_gestionar.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                btnCliente.Visible = false;
                btnventa.Visible = false;
                btn_almacen_producto.Enabled = false;
                btn_almacen_producto.Visible = false;
                button5.Location = new Point(4, 108);
            }
            else if (ValidarRango == "3") {
                button1.Enabled = false;
                button2.Enabled = false;
                btn_gestionar.Enabled = false;
                button3.Enabled = false;
                button4.Enabled = false;
                btnCliente.Enabled = false;
 
                button1.Visible = false;
                button2.Visible = false;
                btn_gestionar.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                btnCliente.Visible = false;
              
                btn_gestionar_proveedor.Enabled = false;
                btn_gestionar_proveedor.Visible = false;
                btn_pedido_proveedor.Enabled = false;
                btn_pedido_proveedor.Visible = false;
                btn_producto.Visible = false;
                btn_producto.Enabled = false;

                btn_almacen_producto.Visible = false;
                btn_almacen_producto.Enabled = false;

                button5.Visible = false;
                button5.Enabled = false;
                btnventa.Location = new Point(4, 68);
               btn_historial_ventas.Location = new Point(4, 98);

            }

        }
        
        private void abrirFrmPedido_proveedor( )
        {
            Detalle_Producto_proveedor form = new Detalle_Producto_proveedor();

            form.txtidprincipal.Text = lblcodigo.Text;
                
                if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;
      
            hijo1.Show();
        }
        

        private void btn_pedido_proveedor_Click(object sender, EventArgs e)
        {

            abrirFrmPedido_proveedor( );

         
        }
        private void abriralmacen(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            abrirFrmPedido_proveedor();
        }
        private void abrirproveedor(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            abrirproveedor(new ProveedorForm());
        }
        private void abrirproducto(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void btn_producto_Click(object sender, EventArgs e)
        {
            abrirproducto(new ProductoForm());
        }
        private void abrirAlmacen_producto(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void btn_almacen_producto_Click(object sender, EventArgs e)
        {
            abrirAlmacen_producto(new Detalle_producto_almacen());
        }
        private void abrirEmpleado(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            abrirEmpleado(new frmEmpleado());
        }
        private void abrirciudadpais(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void btn_gestionar_Click(object sender, EventArgs e)
        {
            abrirciudadpais(new AgregarPaisForm());
        }
        private void abrircategorias(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void button3_Click_1(object sender, EventArgs e)
        {
            abrircategorias(new AgregarCategoriaForm());
        }
        private void abrirsucursal(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            abrirsucursal(new RegistroSucursal());
        }
        private void historial_entregas(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            historial_entregas(new frmHistorial_entregas());
        }
        private void Venta( )
        {
            FormularioVenta_Producto form = new FormularioVenta_Producto();
            form.txtidprincipal.Text = lblcodigo.Text;
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
          

        }
        private void GestionarCliente(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void btnCliente_Click(object sender, EventArgs e)
        {
            GestionarCliente(new ClienteForm());
        }

        private void Btn_session_Click(object sender, EventArgs e)
        {
            FrmUsuario f = new FrmUsuario();
            f.Show();
            this.Close();
        }

        private void btnventa_Click(object sender, EventArgs e)
        {
            Venta();
        }
        private void gestionerHistorialVentas(object form)
        {
            if (this.metroPanel2.Controls.Count > 0)
                metroPanel2.Controls.RemoveAt(1);
            Form hijo1 = form as Form;
            hijo1.TopLevel = false;
            hijo1.FormBorderStyle = 0;
            hijo1.Dock = DockStyle.Fill;
            this.metroPanel2.Controls.Add(hijo1);
            this.metroPanel2.Tag = hijo1;

            hijo1.Show();
        }
        private void btn_historial_ventas_Click(object sender, EventArgs e)
        {
            gestionerHistorialVentas(new HistorialVentasCliente());
        }
    }
}
