﻿namespace Almacen_Farmacorp
{
    partial class HistorialVentasCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txtbuscarnit = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(13, 99);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(759, 505);
            this.dataGridView1.TabIndex = 0;
            // 
            // txtbuscarnit
            // 
            this.txtbuscarnit.Location = new System.Drawing.Point(13, 56);
            this.txtbuscarnit.Name = "txtbuscarnit";
            this.txtbuscarnit.Size = new System.Drawing.Size(259, 22);
            this.txtbuscarnit.TabIndex = 1;
            this.txtbuscarnit.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtbuscarnit_KeyUp);
            // 
            // HistorialVentasCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 616);
            this.Controls.Add(this.txtbuscarnit);
            this.Controls.Add(this.dataGridView1);
            this.Name = "HistorialVentasCliente";
            this.Text = "HistorialVentasCliente";
            this.Load += new System.EventHandler(this.HistorialVentasCliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox txtbuscarnit;
    }
}