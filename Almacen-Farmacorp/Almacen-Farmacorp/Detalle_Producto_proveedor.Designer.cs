﻿namespace Almacen_Farmacorp
{
    partial class Detalle_Producto_proveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtfecha = new System.Windows.Forms.TextBox();
            this.dtTraerProducto = new System.Windows.Forms.DataGridView();
            this.Agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Cantidad_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioUnitario_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Centavos_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtDetalle_prod = new System.Windows.Forms.DataGridView();
            this.Quitar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.IdProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Subtotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtmontopedido = new System.Windows.Forms.TextBox();
            this.cbxProveedor = new System.Windows.Forms.ComboBox();
            this.btnAgregar = new MetroFramework.Controls.MetroButton();
            this.cbxEstadopedido = new System.Windows.Forms.ComboBox();
            this.txtidpedido = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtidprincipal = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtTraerProducto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDetalle_prod)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 80);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha pedido";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 124);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Total monto pedido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(50, 164);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Estado de pedido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 206);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Proveedor";
            // 
            // txtfecha
            // 
            this.txtfecha.Enabled = false;
            this.txtfecha.Location = new System.Drawing.Point(263, 80);
            this.txtfecha.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtfecha.Name = "txtfecha";
            this.txtfecha.Size = new System.Drawing.Size(148, 26);
            this.txtfecha.TabIndex = 4;
            // 
            // dtTraerProducto
            // 
            this.dtTraerProducto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtTraerProducto.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Agregar,
            this.Cantidad_,
            this.PrecioUnitario_,
            this.Centavos_});
            this.dtTraerProducto.Location = new System.Drawing.Point(54, 276);
            this.dtTraerProducto.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtTraerProducto.Name = "dtTraerProducto";
            this.dtTraerProducto.Size = new System.Drawing.Size(956, 191);
            this.dtTraerProducto.TabIndex = 7;
            this.dtTraerProducto.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtTraerProducto_CellClick);
            this.dtTraerProducto.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtTraerProducto_CellContentClick);
            this.dtTraerProducto.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dtTraerProducto_EditingControlShowing);
            this.dtTraerProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtTraerProducto_KeyPress);
            this.dtTraerProducto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.dtTraerProducto_KeyUp);
            // 
            // Agregar
            // 
            this.Agregar.HeaderText = "Agregar";
            this.Agregar.Name = "Agregar";
            // 
            // Cantidad_
            // 
            this.Cantidad_.HeaderText = "Cantidad_";
            this.Cantidad_.Name = "Cantidad_";
            // 
            // PrecioUnitario_
            // 
            this.PrecioUnitario_.HeaderText = "PrecioUnitario_";
            this.PrecioUnitario_.Name = "PrecioUnitario_";
            // 
            // Centavos_
            // 
            this.Centavos_.HeaderText = "Centavos_";
            this.Centavos_.Name = "Centavos_";
            // 
            // dtDetalle_prod
            // 
            this.dtDetalle_prod.AllowUserToDeleteRows = false;
            this.dtDetalle_prod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtDetalle_prod.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quitar,
            this.IdProducto,
            this.NombreProducto,
            this.Cantidad,
            this.PrecioUnitario,
            this.Subtotal});
            this.dtDetalle_prod.Location = new System.Drawing.Point(54, 496);
            this.dtDetalle_prod.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtDetalle_prod.Name = "dtDetalle_prod";
            this.dtDetalle_prod.Size = new System.Drawing.Size(956, 191);
            this.dtDetalle_prod.TabIndex = 8;
            this.dtDetalle_prod.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtDetalle_prod_CellClick);
            // 
            // Quitar
            // 
            this.Quitar.HeaderText = "Quitar";
            this.Quitar.Name = "Quitar";
            // 
            // IdProducto
            // 
            this.IdProducto.HeaderText = "IdProducto";
            this.IdProducto.Name = "IdProducto";
            this.IdProducto.ReadOnly = true;
            // 
            // NombreProducto
            // 
            this.NombreProducto.HeaderText = "NombreProducto";
            this.NombreProducto.Name = "NombreProducto";
            this.NombreProducto.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // PrecioUnitario
            // 
            this.PrecioUnitario.HeaderText = "PrecioUnitario";
            this.PrecioUnitario.Name = "PrecioUnitario";
            this.PrecioUnitario.ReadOnly = true;
            // 
            // Subtotal
            // 
            this.Subtotal.HeaderText = "Subtotal";
            this.Subtotal.Name = "Subtotal";
            this.Subtotal.ReadOnly = true;
            // 
            // txtmontopedido
            // 
            this.txtmontopedido.Enabled = false;
            this.txtmontopedido.Location = new System.Drawing.Point(263, 124);
            this.txtmontopedido.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtmontopedido.Name = "txtmontopedido";
            this.txtmontopedido.Size = new System.Drawing.Size(148, 26);
            this.txtmontopedido.TabIndex = 9;
            this.txtmontopedido.Text = "0";
            // 
            // cbxProveedor
            // 
            this.cbxProveedor.FormattingEnabled = true;
            this.cbxProveedor.Location = new System.Drawing.Point(263, 207);
            this.cbxProveedor.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxProveedor.Name = "cbxProveedor";
            this.cbxProveedor.Size = new System.Drawing.Size(180, 28);
            this.cbxProveedor.TabIndex = 11;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(470, 73);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(112, 35);
            this.btnAgregar.TabIndex = 12;
            this.btnAgregar.Text = "Registrar";
            this.btnAgregar.UseSelectable = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // cbxEstadopedido
            // 
            this.cbxEstadopedido.FormattingEnabled = true;
            this.cbxEstadopedido.Items.AddRange(new object[] {
            "envio en transito",
            "envio pendiende de recogida",
            "envio en reparto",
            "Destinatario Ausente",
            "En fase de devolución"});
            this.cbxEstadopedido.Location = new System.Drawing.Point(263, 166);
            this.cbxEstadopedido.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbxEstadopedido.Name = "cbxEstadopedido";
            this.cbxEstadopedido.Size = new System.Drawing.Size(553, 28);
            this.cbxEstadopedido.TabIndex = 10;
            // 
            // txtidpedido
            // 
            this.txtidpedido.Enabled = false;
            this.txtidpedido.Location = new System.Drawing.Point(54, 50);
            this.txtidpedido.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidpedido.Name = "txtidpedido";
            this.txtidpedido.Size = new System.Drawing.Size(148, 26);
            this.txtidpedido.TabIndex = 13;
            this.txtidpedido.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(604, 73);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 35);
            this.button1.TabIndex = 15;
            this.button1.Text = "Productos";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtidprincipal
            // 
            this.txtidprincipal.Enabled = false;
            this.txtidprincipal.Location = new System.Drawing.Point(54, 14);
            this.txtidprincipal.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtidprincipal.Name = "txtidprincipal";
            this.txtidprincipal.Size = new System.Drawing.Size(148, 26);
            this.txtidprincipal.TabIndex = 17;
            this.txtidprincipal.Visible = false;
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(628, 236);
            this.txtnombre.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(380, 26);
            this.txtnombre.TabIndex = 18;
            this.txtnombre.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtnombre_KeyUp);
            // 
            // Detalle_Producto_proveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1586, 998);
            this.Controls.Add(this.txtnombre);
            this.Controls.Add(this.txtidprincipal);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtidpedido);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.cbxProveedor);
            this.Controls.Add(this.cbxEstadopedido);
            this.Controls.Add(this.txtmontopedido);
            this.Controls.Add(this.dtDetalle_prod);
            this.Controls.Add(this.dtTraerProducto);
            this.Controls.Add(this.txtfecha);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Detalle_Producto_proveedor";
            this.Text = "Detalle de pedido";
            this.Load += new System.EventHandler(this.Detalle_Producto_proveedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtTraerProducto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDetalle_prod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtfecha;
        private System.Windows.Forms.DataGridView dtTraerProducto;
        private System.Windows.Forms.DataGridView dtDetalle_prod;
        private System.Windows.Forms.TextBox txtmontopedido;
        private System.Windows.Forms.ComboBox cbxProveedor;
        private MetroFramework.Controls.MetroButton btnAgregar;
        private System.Windows.Forms.ComboBox cbxEstadopedido;
        private System.Windows.Forms.DataGridViewButtonColumn Quitar;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Subtotal;
        private System.Windows.Forms.TextBox txtidpedido;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.TextBox txtidprincipal;
        private System.Windows.Forms.DataGridViewButtonColumn Agregar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioUnitario_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Centavos_;
        private System.Windows.Forms.TextBox txtnombre;
    }
}