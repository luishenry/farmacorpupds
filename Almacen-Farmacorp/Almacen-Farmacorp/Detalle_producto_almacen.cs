﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class Detalle_producto_almacen : Form
    {
        public Detalle_producto_almacen()
        {
            InitializeComponent();
        }
        string f;
        public string fechaCambio="MM/dd/yy";

        Capanegocio.Cpedidoproveedor pp = new Capanegocio.Cpedidoproveedor();
        Capacontrol.CControl cc = new Capacontrol.CControl();
        Capanegocio.CAlmacen Cal= new Capanegocio.CAlmacen();
        public void TraerFechaPedido()
        {
            DataTable d = new DataTable();
            d = pp.TraerPedidoCbx(f);
            cbxPedido.DataSource = d;
          
            cbxPedido.DisplayMember = "detalle";
            cbxPedido.ValueMember = "pp.idpedido_proveedor";


        }


        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime fecha = dateTimePicker1.Value;
            f = fecha.ToString(fechaCambio);
            try { TraerFechaPedido(); }
            catch { }
            

        }
        public void traerAlmacen()
        {

            DataTable d = new DataTable();
            d = Cal.almacentraer();
            cbxalmacen.DataSource = d;
            cbxalmacen.DisplayMember = "nombre_almacen";
            cbxalmacen.ValueMember = "idalmacen";
        }
        private void Detalle_producto_almacen_Load(object sender, EventArgs e)
        {
            lblfecha.Text = DateTime.Today.ToString(fechaCambio);
            traerAlmacen();
        }
        public void TraerDatosDtgri() {
            DataTable d = new DataTable();
            d = pp.traerdetallepedido(cbxPedido.SelectedValue.ToString());
            dataGridView1.DataSource = d;
        }


        //detector_ventas
        public void detectarVentas()
        {

            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = pp.detectarVentas();

            foreach (DataRow item in d.Rows)
            {

                l.Add((DataRow)item);

            }
            l = d.AsEnumerable().ToList();
 


            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {

                for (int filas = 0; filas < l.Count; filas++)
                {
                    for (int j = 0; j < 2; j++)
                    {

                        string p = l[filas][j].ToString();

                        if (p == dataGridView1.Rows[i].Cells[6].Value.ToString())
                        {


                          
                            dataGridView1.Rows[i].Cells[1].Value = int.Parse(l[0][j - 1].ToString().Split(',')[0]);
                            try
                            {
                                double vconv = double.Parse(l[0][j - 1].ToString());
                                dataGridView1.Rows[i].Cells[2].Value = int.Parse(vconv.ToString().Split(',')[1]);
                            }
                            catch { dataGridView1.Rows[i].Cells[2].Value = "00"; }
                            dataGridView1.Rows[i].DefaultCellStyle.BackColor = Color.Yellow;


                          

                        }
                        else
                        {

                        }

                    }
                }


            }




        }

        //fin detectorVentas
        private void cbxPedido_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbxPedido.Items.Count > 0)
                {
                    TraerDatosDtgri();
                    detectarVentas();
                }
                else
                {


                }
            }
            catch { }
       
        }
        public bool datosexistencia()
        {
            var tr = cc.iniTR();
            if (insertarExistencia(tr))
            {

                cc.finTR(tr);
                return true;

            }

            return false;
        }
        public int consultaproducto()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = pp.consultarexistencia(idprod,idalmacen);

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            { 
                if (l[0][0].ToString() != "")
                {

                    return int.Parse(l[0][0].ToString());
                }

                else
                {

                    return 0;
                }
            }
            catch
            {

                return 0;
            }

        }
        string idprod, idalmacen;
        public bool insertarExistencia(object tr) {
 
            for (int i = 0; i < dataGridView2.Rows.Count-1; i++)
            {    idprod = dataGridView1.Rows[i].Cells[6].Value.ToString();
                  idalmacen =cbxalmacen.SelectedValue.ToString();
                int comparar = consultaproducto();
                if (comparar > 0)

                {

                    pp.acV[0] = dataGridView2.Rows[i].Cells[4].Value.ToString();
                    pp.acV[1] = comparar.ToString();
                    pp.acV[2] = cbxalmacen.SelectedValue.ToString();
                    pp.acV[3] = dataGridView2.Rows[i].Cells[1].Value.ToString();
                    pp.acV[4] = dataGridView2.Rows[i].Cells[2].Value.ToString().Replace(",",".");
                    if (pp.actualizarexistencia(tr)==0 || pp.actualizarprecios(tr)==0) {
                        cc.deshacerTR(tr);
                        MessageBox.Show("error en actualizar cantidad de la existencia");
                        return false;
                    }
                 
                }
                else
                {
                    
                    pp.VE[0] = dataGridView2.Rows[i].Cells[4].Value.ToString();
                    pp.VE[1] = dataGridView2.Rows[i].Cells[1].Value.ToString();
                    pp.VE[2] = dataGridView2.Rows[i].Cells[3].Value.ToString().Replace(",",".");
                    pp.VE[3] = dataGridView2.Rows[i].Cells[2].Value.ToString().Replace(",", ".");
                    pp.VE[4] = cbxalmacen.SelectedValue.ToString();
                    pp.VE[5] = dataGridView2.Rows[i].Cells[6].Value.ToString();//producto

                    if (pp.insertarExistencia(tr) == 0)
                    {
                        cc.deshacerTR(tr);
                        MessageBox.Show("error en datos de existencia");
                        return false;
                    }


                }



            }
            return true;
        }

        public bool datoactualizarestado()
        {
            var tr = cc.iniTR();
            if(actualizarestado(tr))
            {
                cc.finTR(tr);
                return true;
                
            }

            return false;
        }
        public bool actualizarestado(object tr)
        {
            pp.idped[0] = llavePedido;
            if (pp.actualizarestado(tr)==0) {

                cc.deshacerTR(tr);
                MessageBox.Show("error en datos");
                return false;
            }


            return true;
        }
        bool DatoActualizarCantidad() {
            var tr = cc.iniTR();
            if (ActualizarCantidad(tr)) {
                cc.finTR(tr);
                return true;

            }

            return false;
        }

        
        bool ActualizarCantidad(object tr)
        { 
            for (int i = 0; i <= dataGridView2.RowCount - 2; i++)
            {try
                {
                    pp.vC[0] = dataGridView2.Rows[i].Cells[4].Value.ToString();
                    pp.vC[1] = dataGridView2.Rows[i].Cells[7].Value.ToString();
                    pp.vC[2] = dataGridView2.Rows[i].Cells[6].Value.ToString();
                }
                catch { }
                if (pp.ControlPedidoActualizarCantidad_detalle(tr) == 0) {

                    cc.deshacerTR(tr);
                    MessageBox.Show("error al actualizar cantidad");
                    return false;
                }

            }

            return true;
        }
        bool VerificarCantidad_Pedido() {

            int contador = 0;

            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
          
                d = pp.ControlCantidades_detalle(llavePedido);

                foreach (DataRow item in d.Rows)
                {
                    l.Add((DataRow)item);
                }
                l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != null)
                {

                    for (int i = 0; i <l.Count-1; i++)
                    {

                        contador = contador + int.Parse(l[0][i].ToString());
                    }

                    if (contador == 0)
                    {

                        return true;

                    }
                    else
                    {

                        return false;
                    }

                }

                else
                {

                    return false;
                }

            }
            catch { return false; }

            
        }
        string llavePedido="";
        private void bnt_registrar_Click(object sender, EventArgs e)
        {
           if(datosexistencia())
            {
                try { llavePedido = dataGridView2.Rows[0].Cells[7].Value.ToString(); } catch { }
                if (DatoActualizarCantidad()) {
                
                    lblfecha.Text = DateTime.Today.ToString(fechaCambio);
                    traerAlmacen();
                    dataGridView1.DataSource = null;
                    dataGridView1.ClearSelection();
                    cbxPedido.DataSource = null;
                    cbxPedido.Items.Clear();
                    dataGridView2.DataSource = null;
                    dataGridView2.Rows.Clear();
                    r = -1;
                    dateTimePicker1.Value= DateTime.Today.AddDays(1);
                    btn_actualizar.Visible = true;
                    btnalmacen.Visible = false;
                    bnt_registrar.Visible = false;
                }
            }
        }
        int r = -1;
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.ColumnIndex == 0) {
                try {
                    DateTime date_ = dateFecha_Vencimiento.Value ;
                    string fecha_ = date_.ToString("MM/dd/yy");
                    bool exist = dataGridView2.Rows.Cast<DataGridViewRow>().Any(row => Convert.ToString(row.Cells["idproducto"].Value) == dataGridView1.CurrentRow.Cells["idProducto"].Value.ToString());

                    if (!exist && dataGridView1.CurrentRow.Cells["Precio_peso"].Value != null && dataGridView1.CurrentRow.Cells["Precio_centavos"].Value != null && dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value != null)
                    {




                        //validacion de error inicio
                        try
                        {
                            if (int.Parse(dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value.ToString()) <= int.Parse(dataGridView1.CurrentRow.Cells["Cantidad"].Value.ToString()))
                            {
                                dataGridView2.Rows.Add();
                                r++;

                                dataGridView2.Rows[r].Cells[1].Value = fecha_;
                                dataGridView2.Rows[r].Cells[2].Value = dataGridView1.CurrentRow.Cells["Precio_peso"].Value.ToString() + "." + dataGridView1.CurrentRow.Cells["Precio_centavos"].Value.ToString();
                                dataGridView2.Rows[r].Cells[3].Value = dataGridView1.CurrentRow.Cells["preciounitario"].Value;
                                dataGridView2.Rows[r].Cells[4].Value = dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value;
                                dataGridView2.Rows[r].Cells[5].Value = dataGridView1.CurrentRow.Cells["nombre_producto"].Value;
                                dataGridView2.Rows[r].Cells[6].Value = dataGridView1.CurrentRow.Cells["idproducto"].Value;
                                dataGridView2.Rows[r].Cells[7].Value = dataGridView1.CurrentRow.Cells["idpedido"].Value;

                                dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value = "0";
                            }
                            else {
                                MessageBox.Show("La cantidad a almacenar sobrepasa los limites de la cantidad de pedido");

                            }
                            }

                        catch
                        {
                            r--;
                            MessageBox.Show("datos no validos");
                        }
                    }
                    //validacion de error fin
                    else if (exist)
                    {

                        for (int i = 0; i < dataGridView2.RowCount - 1; i++)
                        {
                            if (dataGridView1.CurrentRow.Cells["idproducto"].Value.ToString() == dataGridView2.Rows[i].Cells[6].Value.ToString())
                            {
                                int comparar = int.Parse(dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value.ToString()) + int.Parse(dataGridView2.Rows[i].Cells[4].Value.ToString());
                                int comparar2 = int.Parse(dataGridView1.CurrentRow.Cells["Cantidad"].Value.ToString());
                                if (comparar <= comparar2)
                                {

                                    dataGridView2.Rows[i].Cells[1].Value = fecha_;
                                    dataGridView2.Rows[i].Cells[2].Value = dataGridView1.CurrentRow.Cells["Precio_peso"].Value.ToString() + "." + dataGridView1.CurrentRow.Cells["Precio_centavos"].Value.ToString();
                                    dataGridView2.Rows[i].Cells[4].Value = (int.Parse(dataGridView2.Rows[r].Cells[4].Value.ToString()) + int.Parse(dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value.ToString())).ToString();

                                    dataGridView1.CurrentRow.Cells["Cantidad_almacenar"].Value = "0";
                                }
                                else
                                {
                                    MessageBox.Show("sobrepasa la cantidad de cantidades del pedido");
                                }
                            }
                        }
                    }
                    else { MessageBox.Show("Ingrese Valones"); }

                }

                catch { MessageBox.Show("datos no validos"); }



            }


        }


        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            Boolean nonNumberEntered;


            nonNumberEntered = true;


            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == 8)
            {
                nonNumberEntered = false;
            }


            if (nonNumberEntered == true)
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }


        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 1)
            {
                e.Control.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxNumeric_KeyPress);

            }
        }

        private void btn_actualizar_Click(object sender, EventArgs e)
        {
            if (VerificarCantidad_Pedido() == true)
            {

                datoactualizarestado();
                btnalmacen.Visible = true;
                bnt_registrar.Visible = true;
                btn_actualizar.Visible = false;
            }
            btnalmacen.Visible = true;
            bnt_registrar.Visible = true;
            btn_actualizar.Visible = false;

        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
             dataGridView2.Rows.RemoveAt(dataGridView2.CurrentRow.Index);
                r--;

            }
        }

        private void btnalmacen_Click(object sender, EventArgs e)
        {
            AlmacenForm fa = new AlmacenForm();
            fa.Show();
        }
    }
}
