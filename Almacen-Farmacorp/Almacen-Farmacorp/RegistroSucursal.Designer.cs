﻿namespace Almacen_Farmacorp
{
    partial class RegistroSucursal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnguardar = new System.Windows.Forms.Button();
            this.btneditar = new System.Windows.Forms.Button();
            this.dtlistasucursal = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombresucursal = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dtlistasucursal)).BeginInit();
            this.SuspendLayout();
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(16, 69);
            this.btnguardar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(160, 59);
            this.btnguardar.TabIndex = 0;
            this.btnguardar.Text = "Guardar";
            this.btnguardar.UseVisualStyleBackColor = true;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // btneditar
            // 
            this.btneditar.Location = new System.Drawing.Point(193, 69);
            this.btneditar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btneditar.Name = "btneditar";
            this.btneditar.Size = new System.Drawing.Size(160, 59);
            this.btneditar.TabIndex = 1;
            this.btneditar.Text = "Editar";
            this.btneditar.UseVisualStyleBackColor = true;
            this.btneditar.Click += new System.EventHandler(this.btneditar_Click);
            // 
            // dtlistasucursal
            // 
            this.dtlistasucursal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtlistasucursal.Location = new System.Drawing.Point(16, 145);
            this.dtlistasucursal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtlistasucursal.Name = "dtlistasucursal";
            this.dtlistasucursal.Size = new System.Drawing.Size(337, 276);
            this.dtlistasucursal.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nombre Sucursal";
            // 
            // txtnombresucursal
            // 
            this.txtnombresucursal.Location = new System.Drawing.Point(16, 39);
            this.txtnombresucursal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtnombresucursal.Name = "txtnombresucursal";
            this.txtnombresucursal.Size = new System.Drawing.Size(226, 22);
            this.txtnombresucursal.TabIndex = 7;
            // 
            // RegistroSucursal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 434);
            this.Controls.Add(this.txtnombresucursal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtlistasucursal);
            this.Controls.Add(this.btneditar);
            this.Controls.Add(this.btnguardar);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "RegistroSucursal";
            this.Text = "RegistroSucursal";
            this.Load += new System.EventHandler(this.RegistroSucursal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtlistasucursal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnguardar;
        private System.Windows.Forms.Button btneditar;
        private System.Windows.Forms.DataGridView dtlistasucursal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombresucursal;
    }
}