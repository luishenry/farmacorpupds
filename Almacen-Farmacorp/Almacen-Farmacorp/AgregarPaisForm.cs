﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class AgregarPaisForm : Form
    {
        Capanegocio.CPais cpais = new Capanegocio.CPais();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        string operacion = "Insertar";
        string idpaisupdate;
        public AgregarPaisForm()
        {
            InitializeComponent();
        }

        private void AgregarPaisForm_Load(object sender, EventArgs e)
        {
            actualizarForm();
        }

        public void actualizarForm()
        {
            TraerPais();
        }
        public void TraerPais()
        {
            DataTable pais = new DataTable();
            pais = cpais.traerPais();
            dataGridView1.DataSource = pais;

        }
        string idpais;
        public void generarCodigoPais()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = cpais.generaridPais();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idpais = l[0][0].ToString();
                }

                else
                {

                    idpais = "1";
                }
            }
            catch
            {
                idpais = "1";
            }

        }
        public bool insertarPais(object tr)
        {
            generarCodigoPais();
            cpais.v[0] = idpais;
            cpais.v[1] = txtpais.Text;
            cpais.v[2] = txtnacionalidad.Text;

            if (cpais.InsertAlmacen(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Proveedor");
                return false;
            }
            return true;
        }
        public bool actualizarPais(object tr, string id)
        {
            cpais.v2[0] = id;
            cpais.v2[1] = txtpais.Text;
            cpais.v2[2] = txtnacionalidad.Text;
            if (cpais.ActualizarAlmacen(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Proveedor");
                return false;
            }
            return true;
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (operacion == "Insertar")
            {
                var tr = CC.iniTR();
                if (insertarPais(tr))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();

                }
            }
            else if (operacion == "editar")
            {
                var tr = CC.iniTR();
                if (actualizarPais(tr, idpaisupdate))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                    operacion = "Insertar";
                }
            }
        }

        private void btneditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                operacion = "editar";
                txtpais.Text = dataGridView1.CurrentRow.Cells["nombre_pais"].Value.ToString();
                txtnacionalidad.Text = dataGridView1.CurrentRow.Cells["nacionalidad"].Value.ToString();
                idpaisupdate = dataGridView1.CurrentRow.Cells["idpais"].Value.ToString();
            }
            else
            {
                MessageBox.Show("debe seleccionar una fila");
            }
        }
    }
}
