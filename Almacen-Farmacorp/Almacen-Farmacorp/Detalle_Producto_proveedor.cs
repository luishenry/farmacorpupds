﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
 
namespace Almacen_Farmacorp
{
    public partial class Detalle_Producto_proveedor : Form
    {
        public Detalle_Producto_proveedor()
        {
            InitializeComponent();
        }

        Capanegocio.CProveedor p = new Capanegocio.CProveedor();
        Capanegocio.CProducto pr = new Capanegocio.CProducto();
        Capanegocio.Cpedidoproveedor cped = new Capanegocio.Cpedidoproveedor();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        public void traerproveedor() {
            DataTable d = new DataTable();
            d = p.traerproveedor();
            cbxProveedor.DataSource = d;
            cbxProveedor.DisplayMember = "nombre_proveedor";
            cbxProveedor.ValueMember = "idproveedor";
        }
        public void traerproductos() {

            DataTable d = new DataTable();
            d = pr.traerproducto(txtnombre.Text);
            dtTraerProducto.DataSource = d;
             

        }

        string idpedido;
        public void generarpedido()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = cped.traeridpedido();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idpedido = l[0][0].ToString();
                }

                else
                {

                    idpedido = "1";
                }
            }
            catch
            {
                idpedido = "1";
            }

        }
        Capanegocio.CUsuario us = new Capanegocio.CUsuario();
        private void Detalle_Producto_proveedor_Load(object sender, EventArgs e)
        {
            txtfecha.Text = DateTime.Today.ToString("MM/dd/yy");
            traerproveedor();
            traerproductos();
            generarpedido();
            txtidpedido.Text = idpedido;

      
       

        }
        int r = -1;
        private void dtTraerProducto_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) {

                bool exist = dtDetalle_prod.Rows.Cast<DataGridViewRow>().Any(row => Convert.ToString(row.Cells["idproducto"].Value) == dtTraerProducto.CurrentRow.Cells["idProducto"].Value.ToString());
                try
                {
                    

                    int traer = int.Parse(dtTraerProducto.CurrentRow.Cells["cantidad_"].Value.ToString());
               int traer1 = int.Parse(dtTraerProducto.CurrentRow.Cells["PrecioUnitario_"].Value.ToString());


               

                  
                        if (traer > 0 && traer1 > 0)
                        {

                            //inicio de calculos de tablas
                            if (!exist)
                            {

                                r++;
                                dtDetalle_prod.Rows.Add();


                                dtDetalle_prod.Rows[r].Cells[1].Value = dtTraerProducto.CurrentRow.Cells["idproducto"].Value;
                                dtDetalle_prod.Rows[r].Cells[2].Value = dtTraerProducto.CurrentRow.Cells["nombre_producto"].Value;


                                dtDetalle_prod.Rows[r].Cells[3].Value = dtTraerProducto.CurrentRow.Cells["cantidad_"].Value;
                                dtDetalle_prod.Rows[r].Cells[4].Value = dtTraerProducto.CurrentRow.Cells["preciounitario_"].Value.ToString()+","+ dtTraerProducto.CurrentRow.Cells["Centavos_"].Value.ToString();
                               
                                try
                                {
                                    for (int i = 0; i <= r; i++)
                                    {
                                        double operacion = double.Parse(dtDetalle_prod.Rows[i].Cells["cantidad"].Value.ToString()) * double.Parse(dtDetalle_prod.Rows[i].Cells["preciounitario"].Value.ToString());
                                        dtDetalle_prod.Rows[i].Cells["subtotal"].Value = operacion;
                                    }

                                    int sumaMontototal = 0;

                                    for (int i = 0; i < dtDetalle_prod.Rows.Count - 1; i++)
                                    {
                                        if (dtDetalle_prod.Rows[i].Cells["Cantidad"].Value != null)
                                        {

                                            sumaMontototal = sumaMontototal + int.Parse(dtDetalle_prod.Rows[i].Cells["Cantidad"].Value.ToString());

                                        }


                                    }
                                    txtmontopedido.Text = sumaMontototal.ToString();
                                }
                                catch { }

                            }
                            else
                            {
                                if (exist)
                                {
                                    try
                                    {

                                        for (int j = 0; j < dtDetalle_prod.Rows.Count - 1; j++)
                                        {
                                            if (dtDetalle_prod.Rows[j].Cells["IdProducto"].Value.ToString() == dtTraerProducto.CurrentRow.Cells["idproducto"].Value.ToString())

                                            {
                                                dtDetalle_prod.Rows[j].Cells["cantidad"].Value = Convert.ToInt32(dtDetalle_prod.Rows[j].Cells["cantidad"].Value.ToString()) + Convert.ToInt32(dtTraerProducto.CurrentRow.Cells["cantidad_"].Value.ToString());
                                                dtDetalle_prod.Rows[j].Cells["preciounitario"].Value = dtTraerProducto.CurrentRow.Cells["preciounitario_"].Value.ToString() + "," + dtTraerProducto.CurrentRow.Cells["Centavos_"].Value.ToString();

                                                for (int i = 0; i <= r; i++)
                                                {

                                                    dtDetalle_prod.Rows[j].Cells["subtotal"].Value = double.Parse(dtDetalle_prod.Rows[j].Cells["cantidad"].Value.ToString()) * double.Parse(dtDetalle_prod.Rows[j].Cells["preciounitario"].Value.ToString());
                                                }
                                            }
                                        }


                                    }
                                    catch
                                    {
                                        MessageBox.Show("seleccione la columna que desea modificar datos");
                                    }
                                }


                            }
                            //fin
                        }
                        else { MessageBox.Show("Ingrese valores mayores a 0"); }

               
                   

            }
                catch { MessageBox.Show("valores no validos"); }



            }


           

            else if (e.ColumnIndex == 3 || e.ColumnIndex ==4) {


                dtTraerProducto.CurrentRow.Cells["idproducto"].ReadOnly = true;
                dtTraerProducto.CurrentRow.Cells["nombre_producto"].ReadOnly = true;

            


            }

        }
        public bool datosPedido()
        {
            var tr = CC.iniTR();
            if (insertarpedido(tr))
            {
                CC.finTR(tr);
                return true;
            }
            return false;

        }
        public bool datosdetallepedido()
        {
            var tr = CC.iniTR();
            if (insertardetallepedido(tr))
            {
                CC.finTR(tr);
                return true;
            }
            return false;

        }

        public int idempleadogenerado()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = us.traerid(txtidprincipal.Text);

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    return int.Parse(l[0][0].ToString());
                }

                else
                {

                    return 0;
                }
            }
            catch
            {
                return 0;
            }

        }

        public bool insertarpedido(object tr) {
            FrmPrincipal f = new FrmPrincipal();
            cped.v1[0] = txtidpedido.Text;
            cped.v1[1] = txtfecha.Text;
            cped.v1[2] = txtmontopedido.Text;
            cped.v1[3] = cbxEstadopedido.SelectedItem.ToString();
            cped.v1[4] = cbxProveedor.SelectedValue.ToString();
            cped.v1[5] = idempleadogenerado().ToString();
 
            if(cped.insertarpedido(tr)==0){

                MessageBox.Show("error en datos de pedido");
                return false;
            }

            return true;
        }
        public bool insertardetallepedido(object tr) { 
      
        for(int i = 0; i <=r; i++){

            cped.v2[0] = dtDetalle_prod.Rows[i].Cells[3].Value.ToString();
                string reemplazar1, reemplazar2;


                reemplazar1 = dtDetalle_prod.Rows[i].Cells[4].Value.ToString().Replace(",",".");
      

                reemplazar2 = dtDetalle_prod.Rows[i].Cells[5].Value.ToString().Replace(",", ".");
              

                cped.v2[1] = reemplazar1;
            cped.v2[2] = reemplazar2;
            cped.v2[3] = txtidpedido.Text;

       

                cped.v2[4] = dtDetalle_prod.Rows[i].Cells[1].Value.ToString();




                if (cped.insertardetallepedido(tr) == 0) {
                MessageBox.Show("error en los datos de detalle_pedido");
                return false;
            }

        }
            
        
      
        
        return true;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                
              
              
                    if (datosPedido())
                    {
                        datosdetallepedido();
                        generarpedido();

                        txtidpedido.Text = idpedido;
                        MessageBox.Show("registrado");
                    }
                
               
            }

            catch { MessageBox.Show("valores no validos"); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form formu = new ProductoForm();
            formu.Show();
        }

        private void dtTraerProducto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dtDetalle_prod_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0) {

                dtDetalle_prod.Rows.RemoveAt(dtDetalle_prod.CurrentRow.Index);
                r--;

            }
        }

        private void dtTraerProducto_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if ((int)(((System.Windows.Forms.DataGridView)(sender)).CurrentCell.ColumnIndex) == 1)
            {
                e.Control.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextboxNumeric_KeyPress);

            }
        }

        private void TextboxNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            Boolean nonNumberEntered;


            nonNumberEntered = true;


            if ((e.KeyChar >= 48 && e.KeyChar <= 57) || e.KeyChar == 8)
            {
                nonNumberEntered = false;
            }


            if (nonNumberEntered == true)
            {
                // Stop the character from being entered into the control since it is non-numerical.
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }


        }
 
        private void dtTraerProducto_KeyPress(object sender, KeyPressEventArgs e)
        { 
        }

        private void dtTraerProducto_KeyUp(object sender, KeyEventArgs e)
        {
 
        }

        private void txtnombre_KeyUp(object sender, KeyEventArgs e)
        {
            traerproductos();
        }
    }
}
