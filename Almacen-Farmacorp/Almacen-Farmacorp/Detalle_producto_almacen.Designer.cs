﻿namespace Almacen_Farmacorp
{
    partial class Detalle_producto_almacen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Agregar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Precio_Peso = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio_centavos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_almacenar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbxPedido = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblfecha = new System.Windows.Forms.Label();
            this.bnt_registrar = new System.Windows.Forms.Button();
            this.btnalmacen = new System.Windows.Forms.Button();
            this.cbxalmacen = new System.Windows.Forms.ComboBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.Quitar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio_compra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre_producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProducto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdPedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateFecha_Vencimiento = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_actualizar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Agregar,
            this.Precio_Peso,
            this.Precio_centavos,
            this.Cantidad_almacenar});
            this.dataGridView1.Location = new System.Drawing.Point(7, 129);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1117, 258);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dataGridView1_EditingControlShowing);
            // 
            // Agregar
            // 
            this.Agregar.HeaderText = "Agregar";
            this.Agregar.Name = "Agregar";
            // 
            // Precio_Peso
            // 
            this.Precio_Peso.HeaderText = "Precio_Peso";
            this.Precio_Peso.Name = "Precio_Peso";
            // 
            // Precio_centavos
            // 
            this.Precio_centavos.HeaderText = "Precio_centavos";
            this.Precio_centavos.Name = "Precio_centavos";
            this.Precio_centavos.Width = 150;
            // 
            // Cantidad_almacenar
            // 
            this.Cantidad_almacenar.HeaderText = "Cantidad_almacenar";
            this.Cantidad_almacenar.Name = "Cantidad_almacenar";
            this.Cantidad_almacenar.Width = 200;
            // 
            // cbxPedido
            // 
            this.cbxPedido.FormattingEnabled = true;
            this.cbxPedido.Location = new System.Drawing.Point(14, 65);
            this.cbxPedido.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbxPedido.Name = "cbxPedido";
            this.cbxPedido.Size = new System.Drawing.Size(692, 28);
            this.cbxPedido.TabIndex = 2;
            this.cbxPedido.SelectedIndexChanged += new System.EventHandler(this.cbxPedido_SelectedIndexChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(12, 22);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // lblfecha
            // 
            this.lblfecha.AutoSize = true;
            this.lblfecha.Location = new System.Drawing.Point(631, 28);
            this.lblfecha.Name = "lblfecha";
            this.lblfecha.Size = new System.Drawing.Size(75, 20);
            this.lblfecha.TabIndex = 4;
            this.lblfecha.Text = "txt_fecha";
            // 
            // bnt_registrar
            // 
            this.bnt_registrar.Location = new System.Drawing.Point(436, 663);
            this.bnt_registrar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bnt_registrar.Name = "bnt_registrar";
            this.bnt_registrar.Size = new System.Drawing.Size(144, 52);
            this.bnt_registrar.TabIndex = 5;
            this.bnt_registrar.Text = "Registrar";
            this.bnt_registrar.UseVisualStyleBackColor = true;
            this.bnt_registrar.Click += new System.EventHandler(this.bnt_registrar_Click);
            // 
            // btnalmacen
            // 
            this.btnalmacen.Location = new System.Drawing.Point(280, 663);
            this.btnalmacen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnalmacen.Name = "btnalmacen";
            this.btnalmacen.Size = new System.Drawing.Size(144, 51);
            this.btnalmacen.TabIndex = 6;
            this.btnalmacen.Text = "Almacen";
            this.btnalmacen.UseVisualStyleBackColor = true;
            this.btnalmacen.Click += new System.EventHandler(this.btnalmacen_Click);
            // 
            // cbxalmacen
            // 
            this.cbxalmacen.FormattingEnabled = true;
            this.cbxalmacen.Location = new System.Drawing.Point(16, 663);
            this.cbxalmacen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbxalmacen.Name = "cbxalmacen";
            this.cbxalmacen.Size = new System.Drawing.Size(213, 28);
            this.cbxalmacen.TabIndex = 7;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quitar,
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.Precio_compra,
            this.Cantidad_,
            this.Nombre_producto,
            this.idProducto,
            this.IdPedido});
            this.dataGridView2.Location = new System.Drawing.Point(7, 391);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowTemplate.Height = 28;
            this.dataGridView2.Size = new System.Drawing.Size(1117, 231);
            this.dataGridView2.TabIndex = 8;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // Quitar
            // 
            this.Quitar.HeaderText = "Quitar";
            this.Quitar.Name = "Quitar";
            this.Quitar.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Quitar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Fecha_vencimiento";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Precio_venta";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 120;
            // 
            // Precio_compra
            // 
            this.Precio_compra.HeaderText = "Precio_compra";
            this.Precio_compra.Name = "Precio_compra";
            this.Precio_compra.Width = 120;
            // 
            // Cantidad_
            // 
            this.Cantidad_.HeaderText = "Cantidad_";
            this.Cantidad_.Name = "Cantidad_";
            // 
            // Nombre_producto
            // 
            this.Nombre_producto.HeaderText = "Nombre_Producto";
            this.Nombre_producto.Name = "Nombre_producto";
            this.Nombre_producto.Width = 200;
            // 
            // idProducto
            // 
            this.idProducto.HeaderText = "idProducto";
            this.idProducto.Name = "idProducto";
            // 
            // IdPedido
            // 
            this.IdPedido.HeaderText = "idPedido";
            this.IdPedido.Name = "IdPedido";
            // 
            // dateFecha_Vencimiento
            // 
            this.dateFecha_Vencimiento.Location = new System.Drawing.Point(874, 90);
            this.dateFecha_Vencimiento.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateFecha_Vencimiento.Name = "dateFecha_Vencimiento";
            this.dateFecha_Vencimiento.Size = new System.Drawing.Size(200, 26);
            this.dateFecha_Vencimiento.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(870, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "seleccione la fecha de vencimiento";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 641);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Seleccione el Almacen";
            // 
            // btn_actualizar
            // 
            this.btn_actualizar.Location = new System.Drawing.Point(600, 662);
            this.btn_actualizar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_actualizar.Name = "btn_actualizar";
            this.btn_actualizar.Size = new System.Drawing.Size(144, 52);
            this.btn_actualizar.TabIndex = 12;
            this.btn_actualizar.Text = "Nuevo";
            this.btn_actualizar.UseVisualStyleBackColor = true;
            this.btn_actualizar.Visible = false;
            this.btn_actualizar.Click += new System.EventHandler(this.btn_actualizar_Click);
            // 
            // Detalle_producto_almacen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 744);
            this.Controls.Add(this.btn_actualizar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateFecha_Vencimiento);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.cbxalmacen);
            this.Controls.Add(this.btnalmacen);
            this.Controls.Add(this.bnt_registrar);
            this.Controls.Add(this.lblfecha);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cbxPedido);
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Detalle_producto_almacen";
            this.Text = "Detalle_producto_almacen";
            this.Load += new System.EventHandler(this.Detalle_producto_almacen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cbxPedido;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label lblfecha;
        private System.Windows.Forms.Button bnt_registrar;
        private System.Windows.Forms.Button btnalmacen;
        private System.Windows.Forms.ComboBox cbxalmacen;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DateTimePicker dateFecha_Vencimiento;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewButtonColumn Agregar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio_Peso;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio_centavos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_almacenar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewButtonColumn Quitar;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio_compra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdPedido;
        private System.Windows.Forms.Button btn_actualizar;
    }
}