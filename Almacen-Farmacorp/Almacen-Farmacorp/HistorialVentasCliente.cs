﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class HistorialVentasCliente : Form
    {
        Capanegocio.cHistorialVentasCliente obj = new Capanegocio.cHistorialVentasCliente();

        public HistorialVentasCliente()
        {
            InitializeComponent();
        }

        private void HistorialVentasCliente_Load(object sender, EventArgs e)
        {
            traerhisto();
        }
        public void traerhisto()
        {
            DataTable hvc = new DataTable();
            hvc = obj.traerHistorial(txtbuscarnit.Text,txtbuscarnit.Text);
            dataGridView1.DataSource = hvc;

        }
        private void txtbuscarnit_KeyUp(object sender, KeyEventArgs e)
        {
            traerhisto();
        }
    }
}
