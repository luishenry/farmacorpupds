﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class ClienteForm : Form
    {
        Capanegocio.CCliente Ccliente = new Capanegocio.CCliente();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        string operacion = "Insertar";
        string idupdate;
        public ClienteForm()
        {
            InitializeComponent();
        }

        private void ClienteForm_Load(object sender, EventArgs e)
        {
            actualizarForm();
        }
        public void actualizarForm()
        {
            traerCliente();
        }
        public void traerCliente()
        {
            DataTable pro = new DataTable();
            pro = Ccliente.traerCliente();
            dataGridView1.DataSource = pro;

        }

        string idclie;
        public void generarCodigoProveedor()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = Ccliente.generaridCliente();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idclie = l[0][0].ToString();
                }

                else
                {

                    idclie = "1";
                }
            }
            catch
            {
                idclie = "1";
            }

        }
        public bool insertarClient(object tr)
        {
            generarCodigoProveedor();
            Ccliente.v[0] = idclie;
            Ccliente.v[1] = txtname.Text;
            Ccliente.v[2] = txtpapaterno.Text;
            Ccliente.v[3] = txtapmaterno.Text;
            Ccliente.v[4] = txtfecha.Text;
            Ccliente.v[5] = txtdireccion.Text;
            Ccliente.v[6] = txtnit.Text;
            if (Ccliente.insertarCliente(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Cliente");
                return false;
            }
            return true;
        }
        public bool actualizarCliente(object tr, string id)
        {
            Ccliente.v2[0] = id;
            Ccliente.v2[1] = txtname.Text;
            Ccliente.v2[2] = txtpapaterno.Text;
            Ccliente.v2[3] = txtapmaterno.Text;
            Ccliente.v2[4] = txtfecha.Text;
            Ccliente.v2[5] = txtdireccion.Text;
            Ccliente.v2[6] = txtnit.Text;
            if (Ccliente.ActualizarCliente(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Cliente");
                return false;
            }
            return true;
        }
        private void limpiartxt()
        {
            txtname.Clear();
            txtpapaterno.Clear();
            txtapmaterno.Clear();
            txtfecha.Clear();
            txtdireccion.Clear();
            txtnit.Clear();
        }
        private void btnguardar_Click(object sender, EventArgs e)
        {

            if (operacion == "Insertar")
            {
                var tr = CC.iniTR();
                if (insertarClient(tr))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                    limpiartxt();
                }
            }
            else if (operacion == "editar")
            {
                var tr = CC.iniTR();
                if (actualizarCliente(tr, idupdate))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                    operacion = "Insertar";
                    limpiartxt();
                }
            }
        }

        private void btnedit_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                operacion = "editar";
                txtname.Text = dataGridView1.CurrentRow.Cells["nombreCliente"].Value.ToString();
                txtpapaterno.Text = dataGridView1.CurrentRow.Cells["ap_paterno"].Value.ToString();
                txtapmaterno.Text = dataGridView1.CurrentRow.Cells["ap_materno"].Value.ToString();
                txtfecha.Text = dataGridView1.CurrentRow.Cells["fecha_nacimiento"].Value.ToString();
                txtdireccion.Text = dataGridView1.CurrentRow.Cells["direccion"].Value.ToString();
                txtnit.Text = dataGridView1.CurrentRow.Cells["nit"].Value.ToString();
                idupdate = dataGridView1.CurrentRow.Cells["idCliente"].Value.ToString();
            }
            else
            {
                MessageBox.Show("debe seleccionar una fila");
            }
        }
    }
}
