﻿namespace Almacen_Farmacorp
{
    partial class FormularioVenta_Producto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_nuevo = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_nit_cliente = new System.Windows.Forms.TextBox();
            this.txtidprincipal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtfecha = new System.Windows.Forms.TextBox();
            this.Btn_registrar = new System.Windows.Forms.Button();
            this.dtGridCarrito = new System.Windows.Forms.DataGridView();
            this.Quitar = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Nombre_producto_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Precio_venta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontoTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idPedido_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idAlmacen_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idProducto_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtgridProductos = new System.Windows.Forms.DataGridView();
            this.Agregar_carrito = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtbuscarproducto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmonto = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtidpedido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcliente = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BtnAgregarCliente = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtGridCliente = new System.Windows.Forms.DataGridView();
            this.Seleccionar_Cliente = new System.Windows.Forms.DataGridViewButtonColumn();
            this.txtbuscarnit = new System.Windows.Forms.TextBox();
            this.groupFactura = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_cod_autorizacion = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtnro_autorizacion = new System.Windows.Forms.TextBox();
            this.txtNit_empresa = new System.Windows.Forms.TextBox();
            this.txtnro_factura = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridCarrito)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridProductos)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridCliente)).BeginInit();
            this.groupFactura.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_nuevo);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txt_nit_cliente);
            this.groupBox1.Controls.Add(this.txtidprincipal);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtfecha);
            this.groupBox1.Controls.Add(this.Btn_registrar);
            this.groupBox1.Controls.Add(this.dtGridCarrito);
            this.groupBox1.Controls.Add(this.dtgridProductos);
            this.groupBox1.Controls.Add(this.txtbuscarproducto);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtmonto);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtidpedido);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtcliente);
            this.groupBox1.Location = new System.Drawing.Point(27, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(878, 762);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Venta";
            // 
            // btn_nuevo
            // 
            this.btn_nuevo.Location = new System.Drawing.Point(237, 698);
            this.btn_nuevo.Name = "btn_nuevo";
            this.btn_nuevo.Size = new System.Drawing.Size(166, 46);
            this.btn_nuevo.TabIndex = 18;
            this.btn_nuevo.Text = "Nuevo";
            this.btn_nuevo.UseVisualStyleBackColor = true;
            this.btn_nuevo.Click += new System.EventHandler(this.btn_nuevo_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(400, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Nit_Cliente";
            // 
            // txt_nit_cliente
            // 
            this.txt_nit_cliente.Enabled = false;
            this.txt_nit_cliente.Location = new System.Drawing.Point(492, 71);
            this.txt_nit_cliente.Name = "txt_nit_cliente";
            this.txt_nit_cliente.Size = new System.Drawing.Size(354, 26);
            this.txt_nit_cliente.TabIndex = 16;
            // 
            // txtidprincipal
            // 
            this.txtidprincipal.Enabled = false;
            this.txtidprincipal.Location = new System.Drawing.Point(165, 25);
            this.txtidprincipal.Name = "txtidprincipal";
            this.txtidprincipal.Size = new System.Drawing.Size(100, 26);
            this.txtidprincipal.TabIndex = 14;
            this.txtidprincipal.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 452);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 20);
            this.label7.TabIndex = 13;
            this.label7.Text = "Carrito de Compras";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(392, 114);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Fecha/hora";
            // 
            // txtfecha
            // 
            this.txtfecha.Enabled = false;
            this.txtfecha.Location = new System.Drawing.Point(492, 109);
            this.txtfecha.Name = "txtfecha";
            this.txtfecha.Size = new System.Drawing.Size(362, 26);
            this.txtfecha.TabIndex = 11;
            // 
            // Btn_registrar
            // 
            this.Btn_registrar.Location = new System.Drawing.Point(34, 698);
            this.Btn_registrar.Name = "Btn_registrar";
            this.Btn_registrar.Size = new System.Drawing.Size(166, 46);
            this.Btn_registrar.TabIndex = 10;
            this.Btn_registrar.Text = "Registrar";
            this.Btn_registrar.UseVisualStyleBackColor = true;
            this.Btn_registrar.Click += new System.EventHandler(this.Btn_registrar_Click);
            // 
            // dtGridCarrito
            // 
            this.dtGridCarrito.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridCarrito.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Quitar,
            this.Nombre_producto_,
            this.Cantidad_,
            this.Precio_venta,
            this.MontoTotal,
            this.idPedido_,
            this.idAlmacen_,
            this.idProducto_});
            this.dtGridCarrito.Location = new System.Drawing.Point(34, 483);
            this.dtGridCarrito.Name = "dtGridCarrito";
            this.dtGridCarrito.ReadOnly = true;
            this.dtGridCarrito.RowTemplate.Height = 28;
            this.dtGridCarrito.Size = new System.Drawing.Size(822, 197);
            this.dtGridCarrito.TabIndex = 9;
            this.dtGridCarrito.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtGridCarrito_CellClick);
            // 
            // Quitar
            // 
            this.Quitar.HeaderText = "Quitar";
            this.Quitar.Name = "Quitar";
            this.Quitar.ReadOnly = true;
            // 
            // Nombre_producto_
            // 
            this.Nombre_producto_.HeaderText = "Nombre_producto_";
            this.Nombre_producto_.Name = "Nombre_producto_";
            this.Nombre_producto_.ReadOnly = true;
            this.Nombre_producto_.Width = 150;
            // 
            // Cantidad_
            // 
            this.Cantidad_.HeaderText = "Cantidad_";
            this.Cantidad_.Name = "Cantidad_";
            this.Cantidad_.ReadOnly = true;
            // 
            // Precio_venta
            // 
            this.Precio_venta.HeaderText = "Precio_venta";
            this.Precio_venta.Name = "Precio_venta";
            this.Precio_venta.ReadOnly = true;
            this.Precio_venta.Width = 110;
            // 
            // MontoTotal
            // 
            this.MontoTotal.HeaderText = "MontoTotal";
            this.MontoTotal.Name = "MontoTotal";
            this.MontoTotal.ReadOnly = true;
            // 
            // idPedido_
            // 
            this.idPedido_.HeaderText = "idpedido_";
            this.idPedido_.Name = "idPedido_";
            this.idPedido_.ReadOnly = true;
            // 
            // idAlmacen_
            // 
            this.idAlmacen_.HeaderText = "idAlmacen_";
            this.idAlmacen_.Name = "idAlmacen_";
            this.idAlmacen_.ReadOnly = true;
            // 
            // idProducto_
            // 
            this.idProducto_.HeaderText = "idProducto_";
            this.idProducto_.Name = "idProducto_";
            this.idProducto_.ReadOnly = true;
            // 
            // dtgridProductos
            // 
            this.dtgridProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgridProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Agregar_carrito,
            this.Cantidad});
            this.dtgridProductos.Location = new System.Drawing.Point(34, 212);
            this.dtgridProductos.Name = "dtgridProductos";
            this.dtgridProductos.RowTemplate.Height = 28;
            this.dtgridProductos.Size = new System.Drawing.Size(822, 223);
            this.dtgridProductos.TabIndex = 8;
            this.dtgridProductos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtgridProductos_CellClick);
            this.dtgridProductos.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dtgridProductos_EditingControlShowing);
            // 
            // Agregar_carrito
            // 
            this.Agregar_carrito.HeaderText = "Agregar_carrito";
            this.Agregar_carrito.Name = "Agregar_carrito";
            this.Agregar_carrito.Width = 120;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Width = 90;
            // 
            // txtbuscarproducto
            // 
            this.txtbuscarproducto.Location = new System.Drawing.Point(178, 168);
            this.txtbuscarproducto.Name = "txtbuscarproducto";
            this.txtbuscarproducto.Size = new System.Drawing.Size(464, 26);
            this.txtbuscarproducto.TabIndex = 7;
            this.txtbuscarproducto.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtbuscarproducto_KeyUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Buscar_Producto";
            // 
            // txtmonto
            // 
            this.txtmonto.Enabled = false;
            this.txtmonto.Location = new System.Drawing.Point(165, 57);
            this.txtmonto.Name = "txtmonto";
            this.txtmonto.Size = new System.Drawing.Size(100, 26);
            this.txtmonto.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Monto";
            // 
            // txtidpedido
            // 
            this.txtidpedido.Enabled = false;
            this.txtidpedido.Location = new System.Drawing.Point(34, 25);
            this.txtidpedido.Name = "txtidpedido";
            this.txtidpedido.Size = new System.Drawing.Size(100, 26);
            this.txtidpedido.TabIndex = 2;
            this.txtidpedido.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(429, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cliente";
            // 
            // txtcliente
            // 
            this.txtcliente.Enabled = false;
            this.txtcliente.Location = new System.Drawing.Point(494, 32);
            this.txtcliente.Name = "txtcliente";
            this.txtcliente.Size = new System.Drawing.Size(354, 26);
            this.txtcliente.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BtnAgregarCliente);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.dtGridCliente);
            this.groupBox2.Controls.Add(this.txtbuscarnit);
            this.groupBox2.Location = new System.Drawing.Point(910, 29);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(608, 538);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cliente";
            // 
            // BtnAgregarCliente
            // 
            this.BtnAgregarCliente.Location = new System.Drawing.Point(428, 25);
            this.BtnAgregarCliente.Name = "BtnAgregarCliente";
            this.BtnAgregarCliente.Size = new System.Drawing.Size(150, 46);
            this.BtnAgregarCliente.TabIndex = 14;
            this.BtnAgregarCliente.Text = "Agregar nuevo";
            this.BtnAgregarCliente.UseVisualStyleBackColor = true;
            this.BtnAgregarCliente.Click += new System.EventHandler(this.BtnAgregarCliente_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Buscar Nit";
            // 
            // dtGridCliente
            // 
            this.dtGridCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridCliente.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Seleccionar_Cliente});
            this.dtGridCliente.Location = new System.Drawing.Point(14, 78);
            this.dtGridCliente.Name = "dtGridCliente";
            this.dtGridCliente.RowTemplate.Height = 28;
            this.dtGridCliente.Size = new System.Drawing.Size(564, 431);
            this.dtGridCliente.TabIndex = 1;
            this.dtGridCliente.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtGridCliente_CellClick);
            // 
            // Seleccionar_Cliente
            // 
            this.Seleccionar_Cliente.HeaderText = "Seleccionar_Cliente";
            this.Seleccionar_Cliente.Name = "Seleccionar_Cliente";
            this.Seleccionar_Cliente.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Seleccionar_Cliente.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txtbuscarnit
            // 
            this.txtbuscarnit.Location = new System.Drawing.Point(164, 42);
            this.txtbuscarnit.Name = "txtbuscarnit";
            this.txtbuscarnit.Size = new System.Drawing.Size(222, 26);
            this.txtbuscarnit.TabIndex = 0;
            this.txtbuscarnit.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtbuscarnit_KeyUp);
            // 
            // groupFactura
            // 
            this.groupFactura.Controls.Add(this.label13);
            this.groupFactura.Controls.Add(this.txt_cod_autorizacion);
            this.groupFactura.Controls.Add(this.label12);
            this.groupFactura.Controls.Add(this.label11);
            this.groupFactura.Controls.Add(this.label10);
            this.groupFactura.Controls.Add(this.txtnro_autorizacion);
            this.groupFactura.Controls.Add(this.txtNit_empresa);
            this.groupFactura.Controls.Add(this.txtnro_factura);
            this.groupFactura.Location = new System.Drawing.Point(924, 574);
            this.groupFactura.Name = "groupFactura";
            this.groupFactura.Size = new System.Drawing.Size(578, 217);
            this.groupFactura.TabIndex = 15;
            this.groupFactura.TabStop = false;
            this.groupFactura.Text = "Factura";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(129, 20);
            this.label13.TabIndex = 25;
            this.label13.Text = "Nro_autorizacion";
            // 
            // txt_cod_autorizacion
            // 
            this.txt_cod_autorizacion.Enabled = false;
            this.txt_cod_autorizacion.Location = new System.Drawing.Point(174, 185);
            this.txt_cod_autorizacion.Name = "txt_cod_autorizacion";
            this.txt_cod_autorizacion.Size = new System.Drawing.Size(354, 26);
            this.txt_cod_autorizacion.TabIndex = 24;
            this.txt_cod_autorizacion.Text = "EB-4R-6Y-8G-9P";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 188);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(110, 20);
            this.label12.TabIndex = 23;
            this.label12.Text = "CodigoControl";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 108);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(99, 20);
            this.label11.TabIndex = 22;
            this.label11.Text = "Nit_empresa";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 20);
            this.label10.TabIndex = 18;
            this.label10.Text = "Nro_Factura";
            // 
            // txtnro_autorizacion
            // 
            this.txtnro_autorizacion.Enabled = false;
            this.txtnro_autorizacion.Location = new System.Drawing.Point(174, 146);
            this.txtnro_autorizacion.Name = "txtnro_autorizacion";
            this.txtnro_autorizacion.Size = new System.Drawing.Size(354, 26);
            this.txtnro_autorizacion.TabIndex = 20;
            this.txtnro_autorizacion.Text = "7895212556";
            // 
            // txtNit_empresa
            // 
            this.txtNit_empresa.Enabled = false;
            this.txtNit_empresa.Location = new System.Drawing.Point(174, 109);
            this.txtNit_empresa.Name = "txtNit_empresa";
            this.txtNit_empresa.Size = new System.Drawing.Size(354, 26);
            this.txtNit_empresa.TabIndex = 19;
            this.txtNit_empresa.Text = "12598798956";
            // 
            // txtnro_factura
            // 
            this.txtnro_factura.Enabled = false;
            this.txtnro_factura.Location = new System.Drawing.Point(174, 65);
            this.txtnro_factura.Name = "txtnro_factura";
            this.txtnro_factura.Size = new System.Drawing.Size(354, 26);
            this.txtnro_factura.TabIndex = 18;
            this.txtnro_factura.Text = "12569975";
            // 
            // FormularioVenta_Producto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1593, 852);
            this.Controls.Add(this.groupFactura);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormularioVenta_Producto";
            this.Text = "FormularioVenta_Producto";
            this.Load += new System.EventHandler(this.FormularioVenta_Producto_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridCarrito)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridProductos)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridCliente)).EndInit();
            this.groupFactura.ResumeLayout(false);
            this.groupFactura.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtGridCliente;
        private System.Windows.Forms.DataGridViewButtonColumn Seleccionar_Cliente;
        private System.Windows.Forms.TextBox txtbuscarnit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtfecha;
        private System.Windows.Forms.Button Btn_registrar;
        private System.Windows.Forms.DataGridView dtGridCarrito;
        private System.Windows.Forms.DataGridView dtgridProductos;
        private System.Windows.Forms.TextBox txtbuscarproducto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmonto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtidpedido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcliente;
        private System.Windows.Forms.Button BtnAgregarCliente;
        public System.Windows.Forms.TextBox txtidprincipal;
        private System.Windows.Forms.DataGridViewButtonColumn Agregar_carrito;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewButtonColumn Quitar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_producto_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio_venta;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontoTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn idPedido_;
        private System.Windows.Forms.DataGridViewTextBoxColumn idAlmacen_;
        private System.Windows.Forms.DataGridViewTextBoxColumn idProducto_;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_nit_cliente;
        private System.Windows.Forms.GroupBox groupFactura;
        private System.Windows.Forms.TextBox txtnro_autorizacion;
        private System.Windows.Forms.TextBox txtNit_empresa;
        private System.Windows.Forms.TextBox txtnro_factura;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btn_nuevo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txt_cod_autorizacion;
    }
}