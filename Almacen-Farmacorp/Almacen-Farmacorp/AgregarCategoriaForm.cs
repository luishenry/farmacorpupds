﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class AgregarCategoriaForm : Form
    {
        //string operacioncate = "Insertar";
        string idcategoria;
        string idsubcategoria;
        //string operacionsubcate = "Insertar";
        Capanegocio.Ccategoria Ccat = new Capanegocio.Ccategoria();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        public AgregarCategoriaForm()
        {
            InitializeComponent();
        }

        private void AgregarCategoriaForm_Load(object sender, EventArgs e)
        {
            ocultarbotones();
            actualizarForm();
        }
        public void ocultarbotones()
        {
            txtcategoria.Visible = false;
            txtsubcategoria.Visible = false;
            btncategoria.Visible = false;
            btnsubcategoria.Visible = false;

        }
        public void showbtncategoria()
        {
            txtcategoria.Visible = true;
            btncategoria.Visible = true;
        }
        public void showbtnsubcategoria()
        {
            txtsubcategoria.Visible = true;
            btnsubcategoria.Visible = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            showbtncategoria();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            showbtnsubcategoria();
        }
        public void actualizarForm()
        {
            TraerCategoria();
            //TraerSubCategoria(1);
        }
        private void TraerCategoria()
        {
            DataTable cat = new DataTable();
            cat = Ccat.traercategoria();
            cbcategoria.DataSource = cat;
            cbcategoria.DisplayMember = "nombre_categoria";
            cbcategoria.ValueMember = "idcategoria";
        }
        public void generarCodigoCategoria()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = Ccat.generaridCategoria();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idcategoria = l[0][0].ToString();
                }

                else
                {

                    idcategoria = "1";
                }
            }
            catch
            {
                idcategoria = "1";
            }

        }
        public bool insertarCategoria(object tr)
        {
            generarCodigoCategoria();
            Ccat.v[0] = idcategoria;
            Ccat.v[1] = txtcategoria.Text;
            if (Ccat.insertCategoria(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Categoria");
                return false;
            }
            return true;
        }
        private void TraerSubCategoria(int cc)
        {
            DataTable sub = new DataTable();
            sub = Ccat.traersuubcategoria(cc);
            cbsubcategoria.DataSource = sub;
            cbsubcategoria.DisplayMember = "nombre_subcategoria";
            cbsubcategoria.ValueMember = "idsubcategoria";
        }
        public void generarCodigoSubategoria()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = Ccat.generaridsubCategoria();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idsubcategoria = l[0][0].ToString();
                }

                else
                {

                    idsubcategoria = "1";
                }
            }
            catch
            {
                idsubcategoria = "1";
            }

        }
        public bool insertarSubategoria(object tr)
        {
            generarCodigoSubategoria();
            Ccat.vs[0] = idsubcategoria;
            Ccat.vs[1] = txtsubcategoria.Text;
            Ccat.vs[2] = cbcategoria.SelectedValue.ToString();
            if (Ccat.insertsubCategoria(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de subcategoria");
                return false;
            }
            return true;
        }
        
        public void cargarsubcombo()
        {
      
            TraerSubCategoria(int.Parse(cbcategoria.SelectedValue.ToString()));
        }
        private void cbcategoria_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                cargarsubcombo();
            }
            catch
            {

            }
        }

        private void btncategoria_Click(object sender, EventArgs e)
        {
            var tr = CC.iniTR();
            if (insertarCategoria(tr))
            {
                CC.finTR(tr);
                MessageBox.Show("Agregar SubCategorias");
                actualizarForm();
                txtcategoria.Clear();
                ocultarbotones();
                cbsubcategoria.DataSource = null;
                cbsubcategoria.Items.Clear();
            }
        }

        private void btnsubcategoria_Click(object sender, EventArgs e)
        {
            var tr = CC.iniTR();
            if (insertarSubategoria(tr))
            {
                CC.finTR(tr);
                MessageBox.Show("Guardado");
                actualizarForm();
                txtsubcategoria.Clear();
                ocultarbotones();
            }
        }

        private void cbcategoria_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                cargarsubcombo();
            }
            catch
            {

            }
        }
    }
}
