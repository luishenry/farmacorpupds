﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class ProveedorForm : Form
    {
        Capanegocio.CProveedor Cproveedor = new Capanegocio.CProveedor();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        string operacion = "Insertar";
        string idproveupdate;
        public ProveedorForm()
        {
            InitializeComponent();
        }

        private void ProveedorForm_Load(object sender, EventArgs e)
        {
            actualizarForm();
        }
        string idprov;
        public void generarCodigoProveedor()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = Cproveedor.generaridProveedor();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idprov = l[0][0].ToString();
                }

                else
                {

                    idprov = "1";
                }
            }
            catch
            {
                idprov = "1";
            }

        }
        public bool insertarProveedor(object tr)
        {
            generarCodigoProveedor();
            Cproveedor.v[0] = idprov;
            Cproveedor.v[1] = txtname.Text;
            Cproveedor.v[2] = txtdireccion.Text;
            Cproveedor.v[3] = txttelf.Text;
            Cproveedor.v[4] = txtnit.Text;
            Cproveedor.v[5] = txtrptlegal.Text;
            Cproveedor.v[6] = cargarEstado().ToString();
            Cproveedor.v[7] = cbpais.SelectedValue.ToString();
            if (Cproveedor.insertProveedor(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Proveedor");
                return false;
            }
            return true;
        }
        public bool actualizarProveedor(object tr, string id)
        {
            Cproveedor.v2[0] = id;
            Cproveedor.v2[1] = txtname.Text;
            Cproveedor.v2[2] = txtdireccion.Text;
            Cproveedor.v2[3] = txttelf.Text;
            Cproveedor.v2[4] = txtnit.Text;
            Cproveedor.v2[5] = txtrptlegal.Text;
            Cproveedor.v2[6] = cargarEstado().ToString();
            Cproveedor.v2[7] = cbpais.SelectedValue.ToString();
            if (Cproveedor.ActualizarProveedor(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Proveedor");
                return false;
            }
            return true;
        }
        public void actualizarForm()
        {
            TraerProveedor();
            TraerPais();
        }
        public void TraerProveedor()
        {
            DataTable pro = new DataTable();
            pro = Cproveedor.listProveedor();
            dataGridView1.DataSource = pro;

        }
        int i;
        private int cargarEstado()
        {
            try
            {
                if (cbestado.SelectedItem.ToString() == "Activo")
                {
                    i = 1;
                }
                else if (cbestado.SelectedItem.ToString() == "Inactivo")
                {
                    i = 0;
                }

            }
            catch
            {

            }
            return i;
        }
        private void TraerPais()
        {
            DataTable proove = new DataTable();
            proove = Cproveedor.listPais();
            cbpais.DataSource = proove;
            cbpais.DisplayMember = "nombre_pais";
            cbpais.ValueMember = "idpais";
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (operacion == "Insertar")
            {
                var tr = CC.iniTR();
                if (insertarProveedor(tr))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                }
            }
            else if (operacion == "editar")
            {
                var tr = CC.iniTR();
                if (actualizarProveedor(tr, idproveupdate))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                    operacion = "Insertar";
                }
            }
        }

        private void btnactualizar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                operacion = "editar";
                txtname.Text = dataGridView1.CurrentRow.Cells["nombre_proveedor"].Value.ToString();
                txtdireccion.Text = dataGridView1.CurrentRow.Cells["direccoin"].Value.ToString();
                txttelf.Text = dataGridView1.CurrentRow.Cells["telefono"].Value.ToString();
                txtnit.Text = dataGridView1.CurrentRow.Cells["nro_nit"].Value.ToString();
                txtrptlegal.Text = dataGridView1.CurrentRow.Cells["rpte_legal"].Value.ToString();
                //cbestado.Text = dataGridView1.CurrentRow.Cells["estado_proveedor"].Value.ToString();
                cbpais.Text = dataGridView1.CurrentRow.Cells["nombre_pais"].Value.ToString();
                idproveupdate = dataGridView1.CurrentRow.Cells["idProveedor"].Value.ToString();
            }
            else
            {
                MessageBox.Show("debe seleccionar una fila");
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AgregarPaisForm addpais = new AgregarPaisForm();
            addpais.Show();
        }
    }
}
