﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class ProductoForm : Form
    {
        string operacion = "Insertar";
        string idproupdate;
        Capanegocio.CProducto Cpro = new Capanegocio.CProducto();
        public ProductoForm()
        {
            InitializeComponent();
        }

        private void ProductoForm_Load(object sender, EventArgs e)
        {
            actualizarForm();

        }
        public void actualizarForm()
        {
            TraerProductos();
            TraerCategoria();
            TraerSubCategoria();
        }
        Capanegocio.CProducto Cem = new Capanegocio.CProducto();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        string idprod;
        public void generarCodigoProducto()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = Cem.generaridProducto();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idprod = l[0][0].ToString();
                }

                else
                {

                    idprod = "1";
                }
            }
            catch
            {
                idprod = "1";
            }

        }
        public bool insertarProductos(object tr)
        {
            generarCodigoProducto();
            Cem.v[0] = idprod;
            Cem.v[1] = txtname.Text;
            Cem.v[2] = cbsubcategoria.SelectedValue.ToString(); ;
            Cem.v[3] = "2";
            if (Cem.insertarPro(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de empleado");
                return false;
            }
            return true;
        }
        public void TraerProductos()
        {
            DataTable pro = new DataTable();
            pro = Cpro.listProductos();
            dataGridView1.DataSource = pro;

        }
        private void TraerSubCategoria()
        {
            try {  
            DataTable sub = new DataTable();
            sub = Cpro.listSubcategoria(cbcat.SelectedValue.ToString());
            cbsubcategoria.DataSource = sub;
            cbsubcategoria.DisplayMember = "nombre_subcategoria";
            cbsubcategoria.ValueMember = "idsubcategoria"; }
        catch{}
        }
        private void TraerCategoria()
        {
            DataTable cat = new DataTable();
            cat = Cpro.listCategoria();
            cbcat.DataSource = cat;
            cbcat.DisplayMember = "nombre_categoria";
            cbcat.ValueMember = "idcategoria";


        }
        public bool actualizarProducto(object tr, string id)
        {

            Cem.v2[3] = id;
            Cem.v2[0] = txtname.Text;
            Cem.v2[1] = cbsubcategoria.SelectedValue.ToString(); ;
            Cem.v2[2] = "2";
            if (Cem.ActualizarProducto(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de producto");
                return false;
            }
            return true;




        }
        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (operacion == "Insertar")
            {
                var tr = CC.iniTR();
                if (insertarProductos(tr))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                }
            }
            else if (operacion == "editar")
            {
                var tr = CC.iniTR();
                if (actualizarProducto(tr, idproupdate))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    actualizarForm();
                    operacion = "insertar";
                }
            }
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                operacion = "editar";
                txtname.Text = dataGridView1.CurrentRow.Cells["nombre_producto"].Value.ToString();
                cbcat.Text = dataGridView1.CurrentRow.Cells["nombre_categoria"].Value.ToString();
                cbsubcategoria.Text = dataGridView1.CurrentRow.Cells["nombre_subcategoria"].Value.ToString();
                idproupdate = dataGridView1.CurrentRow.Cells["idproducto"].Value.ToString();
            }
            else
            {
                MessageBox.Show("debe seleccionar una fila");
            }
        }
   
        private void cbcat_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
           
                TraerSubCategoria();
            }
            catch
            {

            }
        }

        private void ProductoForm_Load_1(object sender, EventArgs e)
        {
            actualizarForm();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AgregarCategoriaForm fc = new AgregarCategoriaForm();
            fc.Show();
        }
    }
}
