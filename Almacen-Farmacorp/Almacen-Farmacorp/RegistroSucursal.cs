﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class RegistroSucursal : Form
    {
        Capanegocio.Csucursal csucursal = new Capanegocio.Csucursal();
        Capacontrol.CControl CC = new Capacontrol.CControl();
        string operacion = "Insertar";
        string idsucursalupdate;
        public RegistroSucursal()
        {
            InitializeComponent();
        }
        Capanegocio.Csucursal lista = new Capanegocio.Csucursal();

        public void mostrarSucursal ()
        {
            DataTable d = new DataTable();
            d = lista.infoSucursal();
            dtlistasucursal.DataSource = d;
        }

        string idsucursal;
        public void generarcodigoSucursal()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = csucursal.generaridCategoria();

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    idsucursal = l[0][0].ToString();
                }

                else
                {

                    idsucursal = "1";
                }
            }
            catch
            {
                idsucursal = "1";
            }

        }
        public bool insertarSucursal(object tr)
        {
            generarcodigoSucursal();
            csucursal.v[0] = idsucursal;
            csucursal.v[1] = txtnombresucursal.Text;
            if (csucursal.insertarSucursal(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de sucursal");
                return false;
            }
            return true;
        }
        public bool actualizarSucursal(object tr, string id)
        {
            csucursal.v2[0] = id;
            csucursal.v2[1] = txtnombresucursal.Text;
            if (csucursal.ActualizarAlmacen(tr) == 0)
            {
                CC.deshacerTR(tr);

                MessageBox.Show("error en los datos de Sucursal");
                return false;
            }
            return true;
        }
        private void RegistroSucursal_Load(object sender, EventArgs e)
        {
            mostrarSucursal();
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (operacion == "Insertar")
            {
                var tr = CC.iniTR();
                if (insertarSucursal(tr))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    mostrarSucursal();

                }
            }
            else if (operacion == "editar")
            {
                var tr = CC.iniTR();
                if (actualizarSucursal(tr, idsucursalupdate))
                {
                    CC.finTR(tr);
                    MessageBox.Show("Guardado");
                    mostrarSucursal();
                    operacion = "Insertar";
                }
            }
        }

        private void btneditar_Click(object sender, EventArgs e)
        {
            if (dtlistasucursal.SelectedRows.Count > 0)
            {
                operacion = "editar";
                idsucursalupdate = dtlistasucursal.CurrentRow.Cells["idsucursal"].Value.ToString();
                txtnombresucursal.Text = dtlistasucursal.CurrentRow.Cells["nombre_sucursal"].Value.ToString();
            }
            else
            {
                MessageBox.Show("debe seleccionar una fila");
            }
        }
    }
}
