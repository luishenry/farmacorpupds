﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Almacen_Farmacorp
{
    public partial class FrmUsuario : MetroFramework.Forms.MetroForm
    {
        public FrmUsuario()
        {
            InitializeComponent();
        }

        public void cargar_imagen() {

            pictureBox1.Image = Image.FromFile("~/imagenes/login.png");
        }
        Capanegocio.CUsuario CU = new Capanegocio.CUsuario();

        private void FrmUsuario_Load(object sender, EventArgs e)
        {
            lblfecha.Text = DateTime.Today.ToString("dd/MM/yy");
    
        }

        string iduser;
        public void consultarUsuario()
        {
            List<DataRow> l = new List<DataRow>();
            DataTable d = new DataTable();
            d = CU.Verificarusuario(txtlogin.Text, txtpassword.Text); 

            foreach (DataRow item in d.Rows)
            {
                l.Add((DataRow)item);
            }
            l = d.AsEnumerable().ToList();
            try
            {
                if (l[0][0].ToString() != "")
                {

                    iduser = l[0][0].ToString();

                    FrmPrincipal f = new FrmPrincipal();
                    f.lblcodigo.Text = l[0][0].ToString();
                    f.LblUserid.Text= l[0][1].ToString();
                   f.ValidarRango= l[0][2].ToString();
                    f.Show();
                    this.Hide();

                    MessageBox.Show("Bienvenido " + l[0][1].ToString());
                }

                else
                {

                    MessageBox.Show("error de contraseña usuario no existente");
                }
            }
            catch
            {

                MessageBox.Show("error de contraseña usuario no existente");
            }

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            consultarUsuario();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
