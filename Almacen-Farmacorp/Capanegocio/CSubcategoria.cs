﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Capanegocio
{
    public class CSubcategoria:Capadatos.Datos
    {
        public string[] v = new string[11];
        public string[] v2 = new string[11];
        public DataTable traerSubcategoria()
        {
            string s = "select * from Subcategoria";
            return consultar(s);
        }
        public DataTable generaridSubcategoria()
        {
            string s = "select max(idsubcategoria)+1 from Subcategoria";
            return consultar(s);
        }
        public int insertSubcategoria(object tr)
        {
            string s = "insert into Subcategoria values(#v1,'#v2',#v3)";

            s = s.Replace("#v1", v[0]);
            s = s.Replace("#v2", v[1]);
            s = s.Replace("#v3", v[2]);
            return ejecutar(s, tr);
        }
        public int ActualizarSubcategoria(object tr)
        {
            string s = "update Subcategoria set nombre_subcategoria='#v1',idcategoria=#v2 where idsubcategoria=" + v2[0].ToString();

            s = s.Replace("#v1", v2[1]);
            s = s.Replace("#v2", v2[2]);
            return ejecutar(s, tr);
        }
    }
}
