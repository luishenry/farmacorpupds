﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Capanegocio
{
    public class Ccategoria : Capadatos.Datos
    {
        // Para CATEGORIA
        public string[] v = new string[11];
        public string[] v2 = new string[11];
        public DataTable traercategoria()
        {
            string s = "select * from Categoria";
            return consultar(s);
        }
        public DataTable generaridCategoria()
        {
            string s = "select max(idcategoria)+1 from Categoria";
            return consultar(s);
        }
        public int insertCategoria(object tr)
        {
            string s = "insert into Categoria values(#v1,'#v2')";

            s = s.Replace("#v1", v[0]);
            s = s.Replace("#v2", v[1]);
            return ejecutar(s, tr);
        }
        /* public int ActualizarCategoria(object tr)
         {
             string s = "update Categoria set nombre_categoria='#v1' where idProveedor=" + v2[0].ToString();

             s = s.Replace("#v1", v2[1]);

             return ejecutar(s, tr);
         }*/
        //PARA SUBCATEGRORIAS
        public string[] vs = new string[11];
        public string[] vs2 = new string[11];
        public DataTable traersuubcategoria(int i)
        {
            string s = "select * from Subcategoria where idcategoria=" + i;
            return consultar(s);
        }
        public DataTable generaridsubCategoria()
        {
            string s = "select max(idsubcategoria)+1 from Subcategoria";
            return consultar(s);
        }
        public int insertsubCategoria(object tr)
        {
            string s = "insert into Subcategoria values(#v1,'#v2',#v3)";

            s = s.Replace("#v1", vs[0]);
            s = s.Replace("#v2", vs[1]);
            s = s.Replace("#v3", vs[2]);
            return ejecutar(s, tr);
        }/*
        public int ActualizarSubcategoria(object tr)
        {
            string s = "update Subcategoria set nombre_categoria='#v1' where idsubcategoria=" + v2[0].ToString();

            s = s.Replace("#v1", vs2[1]);

            return ejecutar(s, tr);
        }*/
    }
}
