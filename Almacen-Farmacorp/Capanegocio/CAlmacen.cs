﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Capanegocio
{
    public class CAlmacen : Capadatos.Datos
    {
        public string[] v = new string[11];
        public string[] v2 = new string[11];
        
        public DataTable traerAlmacen()
        {
            string s = "select * from Almacen";
            return consultar(s);
        }
        public DataTable almacentraer()
        {
            string s = "select idalmacen,nombre_almacen from Almacen";
            return consultar(s);
        }
        public DataTable generaridAlmacen()
        {
            string s = "select max(idalmacen)+1 from Almacen";
            return consultar(s);
        }
        public int InsertAlmacen(object tr)
        {
            string s = "insert into Almacen values (#v1,'#v2','#v3')";
            s = s.Replace("#v1", v[0]);
            s = s.Replace("#v2", v[1]);
            s = s.Replace("#v3", v[2]);
            return ejecutar(s, tr);
        }
        public int ActualizarAlmacen(object tr)
        {
            string s = "update Almacen set nombre_almacen='#v1',direccion_almacen='#v2' where idalmacen=" + v2[0].ToString();
            s = s.Replace("#v1", v2[1]);
            s = s.Replace("#v2", v2[2]);
            return ejecutar(s, tr);
        }
    }
}
