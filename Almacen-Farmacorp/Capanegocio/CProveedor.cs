﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace Capanegocio
{
    public class CProveedor:Capadatos.Datos
    {
        public string[] v = new string[11];
        public string[] v2 = new string[11];
        public DataTable traerproveedor()
        {

            string s = "select idProveedor, nombre_proveedor from Proveedor";
            return consultar(s);
        }
        public int insertarproveedor(object tr)
        {
            string s = "insert into Proveedor values(2,'','',1,1,'','',1)";
            return ejecutar(s, tr);
        }
        public DataTable listProveedor()
        {
            string s = "select idProveedor, nombre_proveedor, direccoin, telefono,nro_nit,rpte_legal,nombre_pais from Proveedor inner join Pais on Proveedor.idpais = Pais.idpais";
            return consultar(s);
        }
        public DataTable listPais()
        {
            string s = "select * from Pais";
            return consultar(s);
        }
        public DataTable generaridProveedor()
        {
            string s = "select max(idProveedor)+1 from Proveedor";
            return consultar(s);
        }
        public int insertProveedor(object tr)
        {
            string s = "insert into Proveedor values(#v1,'#v2','#v3',#v4,#v5,'#v6',#v7,#v8)";

            s = s.Replace("#v1", v[0]);
            s = s.Replace("#v2", v[1]);
            s = s.Replace("#v3", v[2]);
            s = s.Replace("#v4", v[3]);
            s = s.Replace("#v5", v[4]);
            s = s.Replace("#v6", v[5]);
            s = s.Replace("#v7", v[6]);
            s = s.Replace("#v8", v[7]);


            return ejecutar(s, tr);
        }
        public int ActualizarProveedor(object tr)
        {

            string s = "update Proveedor set nombre_proveedor='#v1', direccoin='#v2', telefono=#v3, nro_nit=#v4, rpte_legal='#v5',estado_proveedor=#v6, idpais=#v7 where idProveedor=" + v2[0].ToString();

            s = s.Replace("#v1", v2[1]);
            s = s.Replace("#v2", v2[2]);
            s = s.Replace("#v3", v2[3]);
            s = s.Replace("#v4", v2[4]);
            s = s.Replace("#v5", v2[5]);
            s = s.Replace("#v6", v2[6]);
            s = s.Replace("#v7", v2[7]);


            return ejecutar(s, tr);
        }
    }
}
