﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace Capanegocio
{
    public class CVentas : Capadatos.Datos
    {
        public DataTable TraerProducto_id_nombre(string nombre, string id) {
            string s = "select e.cantidad_stock as Stock,e.precioventa as Precio_venta,p.nombre_producto as Producto,  e.idproducto as IdProducto, e.idalmacen as IdAlmacen from Existencia_producto e inner join Producto p on p.idproducto=e.idproducto inner join Almacen al on al.idalmacen=e.idalmacen inner join Sucursal s on s.idsucursal=al.idalmacen where al.idalmacen=1 and( p.nombre_producto LIKE '%" + nombre + "%' or p.idproducto like '%" + id + "%')";
            return consultar(s);
        }

        public DataTable GenerarIdPedido() {
            string s = "select max(idpedidocliente)+1 from pedido_cliente";
            return consultar(s);


        }

        public string[] v1 = new string[5];
        public int InsertarPedido(object tr) {
            string s = "insert into pedido_cliente values(#v1, '#v2', #v3, #v4, #v5)";
            s = s.Replace("#v1", v1[0]);
            s = s.Replace("#v2", v1[1]);
            s = s.Replace("#v3", v1[2]);
            s = s.Replace("#v4", v1[3]);
            s = s.Replace("#v5", v1[4]);
            return ejecutar(s, tr);

        }
        public string[] v2 = new string[5];
        public int InsertarDetallePedido(object tr)
        {
            string s = "insert into Detalle_pedidoCliente values(#v1, #v2, #v3, #v4, #v5)";
            s = s.Replace("#v1", v2[0]);
            s = s.Replace("#v2", v2[1]);
            s = s.Replace("#v3", v2[2]);
            s = s.Replace("#v4", v2[3]);
            s = s.Replace("#v5", v2[4]);
            return ejecutar(s, tr);

        }

        public string[] VPed = new string[3];
        public int ActualizarCantidadPedido(object tr) {
            string s = "update Existencia_producto set cantidad_stock= cantidad_stock-#v1 where idalmacen=#v2 and idproducto=#v3";
            s = s.Replace("#v1", VPed[0]);
            s = s.Replace("#v2", VPed[1]);
            s = s.Replace("#v3", VPed[2]);
            return ejecutar(s, tr);
        }

        public DataTable traersucursalid(string id) {
            string s = "select s.idsucursal,s.nombre_sucursal from   Sucursal s inner join empleado e on e.idsucursal= s.idsucursal where e.idempleado="+id;
            return consultar(s);

        }
    } 
}
