﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Capanegocio
{
  public  class Cfactura:Capadatos.Datos
    {
        
     
        public string facturaNro { get; set; }
        public string nitEmpresa { get; set; }
        public string autorizacion { get; set; }
        public string fecha { get; set; }
        public string nitCliente { get; set; }
        public string NombreCliente { get; set; }
        public string Productos { get; set; }
        public string ventas { get; set; }
        public string monto_total { get; set; }
        public byte[] CodigoQr { get; set; }
        public string Codigo_control { get; set; }
    }
}
