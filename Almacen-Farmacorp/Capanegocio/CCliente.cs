﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Capanegocio
{
    public class CCliente : Capadatos.Datos
    {
        public DataTable generaridCliente()
        {
            string s = "select max(idCliente)+1 from Cliente";
            return consultar(s);
        }
        public DataTable traerCliente()
        {
            string s = "select * from Cliente";
            return consultar(s);
        }
        public string[] v = new string[11];
        public int insertarCliente(object tr)
        {

            string s = "insert into Cliente values(#v1,'#v2','#v3','#v4','#v5','#v6',#v7)";

            s = s.Replace("#v1", v[0]);
            s = s.Replace("#v2", v[1]);
            s = s.Replace("#v3", v[2]);
            s = s.Replace("#v4", v[3]);
            s = s.Replace("#v5", v[4]);
            s = s.Replace("#v6", v[5]);
            s = s.Replace("#v7", v[6]);

            return ejecutar(s, tr);
        }
        public string[] v2 = new string[7];
        public int ActualizarCliente(object tr)
        {

            string s = "update Cliente set nombreCliente='#v1',ap_paterno='#v2',ap_materno='#v3',fecha_nacimiento='#v4',direccion='#v5', nit=#v6 where idCliente=" + v2[0].ToString();

            s = s.Replace("#v1", v2[1]);
            s = s.Replace("#v2", v2[2]);
            s = s.Replace("#v3", v2[3]);
            s = s.Replace("#v4", v2[4]);
            s = s.Replace("#v5", v2[5]);
            s = s.Replace("#v6", v2[6]);
            //s = s.Replace("#v7", v[7]);

            return ejecutar(s, tr);
        }
        public DataTable TraerClienteNit(string nit,string nombre) {
            string s = "select idCliente,(nombreCliente+' '+ap_paterno+' '+ap_materno )as Nombre, nit from Cliente where nit Like '%"+nit+"%'or nombreCliente like '%"+nombre+"%'";

            return consultar(s);
        }
    }
}
