﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Capanegocio
{
    public class Cpedidoproveedor : Capadatos.Datos
    {
        public DataTable traeridpedido() {

            string s = "select max(idpedido_proveedor)+1 from PedidoProveedor";
            return consultar(s);
        }

        public string[] v1 = new string[6];
        public int insertarpedido(object tr) {

            string s = "insert into PedidoProveedor values(#v1,'#v2',#v3,'#v4',#v5,#v6)";
            s = s.Replace("#v1", v1[0]);
            s = s.Replace("#v2", v1[1]);
            s = s.Replace("#v3", v1[2]);
            s = s.Replace("#v4", v1[3]);
            s = s.Replace("#v5", v1[4]);
            s = s.Replace("#v6", v1[5]);


            return ejecutar(s, tr);

        }
        public string[] v2 = new string[5];
        public int insertardetallepedido(object tr) {

            string s = "insert into Detalle_pedido values(#v1,#v2,#v3,#v4,#v5)";
            s = s.Replace("#v1", v2[0]);
            s = s.Replace("#v2", v2[1]);
            s = s.Replace("#v3", v2[2]);
            s = s.Replace("#v4", v2[3]);
            s = s.Replace("#v5", v2[4]);
            return ejecutar(s, tr);
        }
        public int actualizarestado_pedido(object tr)
        {
            string s = "";
            return ejecutar(s, tr);
        }
 
        public DataTable traerdetallepedido(string id) {

            string s = "select dd.idpedido,dd.preciounitario,p.idproducto,p.nombre_producto,dd.cantidad from Detalle_pedido dd inner join Producto p on p.idproducto=dd.idproducto where dd.idpedido=" + id;
            return consultar(s);

        }
       

        public DataTable TraerPedidoCbx(string fc) {

            string s = "select pp.idpedido_proveedor,('nombre:'+p.nombre_proveedor+ ', estado: '+ pp.estado_pedido)as detalle from PedidoProveedor pp inner join Proveedor p on p.idProveedor=pp.idproveedor  inner join Detalle_pedido dd on dd.idpedido=pp.idpedido_proveedor where pp.estado_pedido !='Entregado' and pp.fecha_pedido='"+ fc+"'";
            return consultar(s);
        }

    public string[] VE = new string[6];
        public int insertarExistencia(object tr)
        {

            string s = "insert Existencia_producto values(#v1,'#v2',#v3,#v4,#v5,#v6)";
            s = s.Replace("#v1", VE[0]);
            s = s.Replace("#v2", VE[1]);
            s = s.Replace("#v3", VE[2]);
            s = s.Replace("#v4", VE[3]);
            s = s.Replace("#v5", VE[4]);
            s = s.Replace("#v6", VE[5]);
            return ejecutar(s,tr);




        }
 

    

        public DataTable consultarexistencia(string idprod,string idalmacen)
        {
            string s = "select idproducto from Existencia_producto where idproducto="+idprod+" and idalmacen="+idalmacen;

            return consultar(s);
        }

      public string[] acV = new string[5];
        public int actualizarexistencia(object tr)
        {
            string s = "update Existencia_producto set cantidad_stock=cantidad_stock+#v1,fecha_vencimiento='#v4',precioventa=#v5  where idproducto=#v2 and idalmacen=#v3";
            s = s.Replace("#v1",acV[0]);
            s = s.Replace("#v2", acV[1]);
            s = s.Replace("#v3", acV[2]);
            s = s.Replace("#v4", acV[3]);
            s = s.Replace("#v5", acV[4]);
            return ejecutar(s,tr);
        }
        public int actualizarprecios(object tr)
        {
            string s = "update Existencia_producto set  precioventa=#v5  where idproducto=#v2  ";
    
            s = s.Replace("#v2", acV[1]);
         
            s = s.Replace("#v5", acV[4]);
            return ejecutar(s, tr);
        }

        public DataTable historialEntregas1() {

            string s = "select pp.fecha_pedido,pp.estado_pedido,p.nombre_producto from Detalle_pedido dd inner join PedidoProveedor pp on pp.idpedido_proveedor=dd.idpedido inner join Producto p on p.idproducto=dd.idproducto where pp.estado_pedido!='Entregado' order by pp.estado_pedido";
          return consultar(s);

        }
        public DataTable historialEntregas2()
        {

            string s = "select pp.fecha_pedido,pp.estado_pedido,p.nombre_producto from Detalle_pedido dd inner join PedidoProveedor pp on pp.idpedido_proveedor=dd.idpedido inner join Producto p on p.idproducto=dd.idproducto order by pp.estado_pedido";
            return consultar(s);

        }

        //control de actualizacion
       public string[] vC = new string[3];
        public int ControlPedidoActualizarCantidad_detalle(object tr) {

            string s = "update Detalle_pedido set cantidad=cantidad-#v1 where idpedido=#v2 and idproducto=#v3";
            s = s.Replace("#v1", vC[0]);
            s = s.Replace("#v2", vC[1]);
            s = s.Replace("#v3", vC[2]);
            return ejecutar(s,tr);
        }

        public DataTable ControlCantidades_detalle(string idp ) {

            string s = "select cantidad from Detalle_pedido where idpedido="+idp;
            return consultar(s);
        }

        public string[] idped = new string[1];

        public int actualizarestado(object tr)
        {
            string s = "update PedidoProveedor set estado_pedido='Entregado' where idpedido_proveedor=#v1";

            s = s.Replace("#v1", idped[0]);
            return ejecutar(s, tr);

        }
        public DataTable detectarVentas()
        {

            string s = "select precioventa,idproducto from Existencia_producto";

            return consultar(s);
        }

    }
}
